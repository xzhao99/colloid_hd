//
//  polymer_chain.C
//  
//
//  Created by Xujun Zhao on 1/14/16.
//
//

#include <fstream>

#include "libmesh/libmesh_common.h"

#include "pm_periodic_boundary.h"
#include "polymer_chain.h"



namespace libMesh
{


// ======================================================================
PolymerChain::PolymerChain(const std::size_t chain_id)
  : _chain_id(chain_id),
    _periodic_boundary(NULL)
{
  // Do nothing
}


  
// ======================================================================
PolymerChain::PolymerChain(const std::size_t chain_id,
                           PMPeriodicBoundary& pm_pb)
  : _chain_id(chain_id),
  _periodic_boundary(&pm_pb)
{
  // Do nothing
}
  
  

// ======================================================================
PolymerChain::~PolymerChain()
{
  // Free the memory
  for (std::size_t i=0; i<this->n_beads(); ++i)
  {
    if(_beads[i]){
      delete _beads[i];
    }
  }
  
}


// ======================================================================
void PolymerChain::read_data(const std::string& filename)
{
  START_LOG ("read_data()", "PolymerChain");
  
  // Open the local file and check the existance
  std::cout <<"\n###Polymer chain filename = "<<filename <<std::endl;
  std::ifstream infile;
  infile.open (filename, std::ios_base::in);
  if( !infile.good() )
  {
    printf("***warning: read_data() can NOT read the polymer chain data!");
    libmesh_error();
  }
  
  // init variables:
  // point_type:  0 - polymer bead point; 1 - tracking point; or user-defined type
  const PointType point_type = POLYMER_BEAD;
  Real x=0., y=0., z=0.;            // initialize bead coords
  std::size_t n_beads, b_id, c_id, bead_type;   //
  std::vector<Real> rot_vec(4); // rotation vector (a,b,c) + theta.
  
  // read particle data
  infile >> n_beads;            // total number of beads
  _beads.resize(n_beads);
  for (std::size_t i=0; i<n_beads; ++i)
  {
    // We read, but don't use the bead_type when construct PointParticle.
    // because the file numbers the bead and its type from 1, while C++ prefers
    // to start the number from 0.
    infile >> b_id >> c_id >> bead_type >> x >> y >> z
    >> rot_vec[0] >> rot_vec[1] >>rot_vec[2] ;
    Point pt(x-100.0,y,z);
    //Point pt(x,y,z);
    PointParticle* particle = new PointParticle(pt, i, point_type, rot_vec);
    particle->set_parent_id(c_id);  // parent id = chain id
    
    // add to the beads list
    _beads[i] = particle;
    
  } // end for i-loop
  
  // change chain id
  _chain_id = c_id;
  
  // Finish and close the file
  infile.close();
  std::cout << "Reading polymer chain data from "<<filename<<" is completed!\n\n";
  
  STOP_LOG ("read_data()", "PolymerChain");
}


  
// ======================================================================
void PolymerChain::read_data_pizza(const std::string& filename)
{
  START_LOG ("read_data_pizza()", "PolymerChain");
  
  // Open the local file and check the existance
  std::cout <<"\n###Read polymer chain genearated from Pizza.py toolkit"<<std::endl;
  std::cout <<"   filename = "<<filename <<std::endl;
  std::ifstream infile;
  infile.open (filename, std::ios_base::in);
  if( !infile.good() )
  {
    printf("***warning: read_data_pizza() can NOT read the polymer chain data!");
    libmesh_error();
  }
  
  // init variables:
  // point_type:  0 - polymer bead point; 1 - tracking point; or user-defined type
  const PointType point_type = POLYMER_BEAD;
  Real x=0., y=0., z=0.;            // initialize bead coords
  int bead_id, chain_id, bead_type; //
  std::vector<Real> rot_vec(4); // rotation vector (a,b,c) + theta.
  std::size_t n1, n2, n3;
  
  
  // Read file line by line
  std::string line_str, str_tmpt;
  std::getline(infile, line_str); // 0. Header line
  infile >> _n_beads >> line_str; // 1. # of beads
  infile >> _n_bonds >> line_str; // 2. # of bonds
  infile >> _n_bead_types >> line_str >> str_tmpt; // 3. # of bead types
  infile >> _n_bond_types >> line_str >> str_tmpt; // 4. # of bond types
  
  // An alternative way to read line info
//  std::getline(infile, line_str); // 1. # of beads
//  std::istringstream iss1(line_str);
//  iss1 >> _n_beads;
//  std::getline(infile, line_str); // 2. # of bonds
//  std::istringstream iss2(line_str);
//  iss2 >> _n_bonds;
//  std::getline(infile, line_str); // 3. # of bead types
//  std::istringstream iss3(line_str);
//  iss3 >> _n_bead_types;
//  std::getline(infile, line_str); // 4. # of bead types
//  std::istringstream iss4(line_str);
//  iss4 >> _n_bond_types;
  
  // init
  _mass.resize(_n_bead_types);
  _beads.resize(_n_beads);
  _bonds.resize(_n_bonds);
  for(std::size_t i=0; i<_n_bonds; ++i) _bonds[i].resize(3);
  
  // Read the bead mass
  while (std::getline(infile, line_str)){
    if(line_str=="Masses") break;
  }
  std::getline(infile, line_str); // skip the empty line
  for(std::size_t i=0; i<_n_bead_types; ++i)
  {
    infile >> n1 >> _mass[i];
  }
  
  
  // Read the beads
  while (std::getline(infile, line_str)){
    if(line_str=="Atoms") break;
  }
  std::getline(infile, line_str); // skip the empty line
  for(std::size_t i=0; i<_n_beads; ++i)
  {
    // read data in this line
    infile >> bead_id >> chain_id >> bead_type >> x >> y >> z
           >> rot_vec[0] >> rot_vec[1] >>rot_vec[2];
    
    // create PointParticle
    Point pt(x,y,z);
    PointParticle* particle = new PointParticle(pt, bead_id-1, point_type, rot_vec);
    particle->set_parent_id(chain_id-1);  // parent id = chain id
    
    // add to the beads list
    _beads[i] = particle;
  }
  
  
  // Read the bonds
  while (std::getline(infile, line_str)){
    if(line_str=="Bonds") break;
  }
  std::getline(infile, line_str); // skip the empty line
  for(std::size_t i=0; i<_n_bonds; ++i)
  {
    // read data in this line
    infile >> bead_id >> n1 >> n2 >> n3;
    
    // create PointParticle
    _bonds[i][0] = n1-1;  // bond type
    _bonds[i][1] = n2-1;  // connect bead 1
    _bonds[i][2] = n3-1;  // connect bond 2
  }

  
  // Finish and close the file
  infile.close();
  std::cout << "Reading polymer chain data from "<<filename<<" is completed!\n\n";
  
  
  STOP_LOG ("read_data_pizza()", "PolymerChain");
}

  

// ======================================================================
void PolymerChain::read_data_vtk(const std::string& filename)
{
  START_LOG ("read_data_vtk()", "PolymerChain");
  
  // Open the local file and check the existance
  std::cout <<"\n###Read polymer chain with vtk format"<<std::endl;
  std::cout <<"   filename = "<<filename <<std::endl;
  std::ifstream infile;
  infile.open (filename, std::ios_base::in);
  if( !infile.good() )
  {
    printf("***warning: read_data_vtk() can NOT read the polymer chain data!");
    libmesh_error();
  }
  
  // init variables:
  // point_type:  0 - polymer bead point; 1 - tracking point; or user-defined type
  const PointType point_type = POLYMER_BEAD;
  Real x=0., y=0., z=0.;            // initialize bead coords
  int bead_id, chain_id, bead_type; //
  std::vector<Real> rot_vec(4); // rotation vector (a,b,c) + theta.
  std::size_t n1, n2, n3;
  
  
  // Read file line by line
  std::string line_str, str_tmpt;
  std::getline(infile, line_str); // 0. Header line
  std::getline(infile, line_str); // 1. Header line
  std::getline(infile, line_str); // 2. Header line
  std::getline(infile, line_str); // 3. Header line
  infile >> line_str >> _n_beads >> str_tmpt; // 4. # of beads

  // Read beads data
  _beads.resize(_n_beads);
  for(std::size_t i=0; i<_n_beads; ++i)
  {
    // read data in this line: coordinates
    infile >> x >> y >> z;
    
    // create PointParticle
    Point pt(x,y,z);
    bead_type = 0;    // bead type = 0 for polymer chain.
    PointParticle* particle = new PointParticle(pt, i, point_type, rot_vec);
    
    // add to the beads list
    if( _beads[i] ) delete _beads[i];
    _beads[i] = particle;
  }
  
  // Read bonds data
  std::getline(infile, line_str);       // skip the empty line
  infile >> line_str >> _n_bonds >> n1; // # of bonds
  _bonds.resize(_n_bonds);
  for(std::size_t i=0; i<_n_bonds; ++i) _bonds[i].resize(3);
  for(std::size_t i=0; i<_n_bonds; ++i)
  {
    // read data in this line
    infile >> n1 >> n2 >> n3;
    
    // create PointParticle
    _bonds[i][0] = 0;   // bond type is set 0
    _bonds[i][1] = n2;  // connect bead 1
    _bonds[i][2] = n3;  // connect bond 2
  }
  
  // Read parent id
  std::getline(infile, line_str);   // skip(this line can't be removed)
  std::getline(infile, str_tmpt);   // skip
  std::getline(infile, str_tmpt);   // skip
  std::getline(infile, str_tmpt);   // skip
  for(std::size_t i=0; i<_n_beads; ++i)
  {
    infile >> chain_id;
    _beads[i]->set_parent_id(chain_id);
    //printf("---> chain id = %d\n",chain_id);
  }
  
  // Finish and close the file
  infile.close();
  std::cout << "Reading polymer chain data from "<<filename<<" is completed!\n\n";
  
  
  STOP_LOG ("read_data_vtk()", "PolymerChain");
}

  
  
// ======================================================================
void PolymerChain::add_bead(const Point& pt,
                            const PointType point_type,
                            const int parent_id,
                            const std::size_t bond_bead,
                            const std::vector<Real>& rot_vec)
{
  START_LOG ("add_bead()", "PolymerChain");
  
  // bead id = the last id + 1
  const std::size_t bead_id = _beads.size();
  PointParticle* particle = new PointParticle(pt,bead_id,point_type,rot_vec);
  particle->set_parent_id(parent_id);
  
  // add to the beads list
  _beads.push_back(particle);

  // build the bond
  std::vector<std::size_t> new_bond(3);
  new_bond[0] = 0;
  new_bond[1] = bead_id - 1;
  new_bond[2] = bead_id;
  _bonds.push_back(new_bond);
  
  STOP_LOG ("add_bead()", "PolymerChain");
}
  
  
  
// ======================================================================
void PolymerChain::generate_polymer_chain(const Point pt0,
                                          const std::size_t Nb,
                                          const Real Ls,
                                          const Point& bbox_min,
                                          const Point& bbox_max,
                                          const std::string& filename)
{
  // problem dimension and domain size
  const std::size_t dim = 3;
  const Real r = 1.0, den = 1.0;
  const std::size_t Ns = Nb - 1;  // number of springs
  
  // generate random particle coordinates inside the domain, and write out the file
//  if( this->comm().rank()==0 )
  {
    printf("---> test in generate_polymer_chain: Generating %lu beads and %lu springs...\n",Nb,Ns);
    
    // write the particle coordinates into a file
    //std::string filename = "polymer_data.in";
    int o_width = 5, o_precision = 9;
    
    std::ofstream outfile;
    outfile.open(filename, std::ios_base::out);
    outfile << Nb << "\n";
    Point pt1, nv, bv;
    for(std::size_t i=0; i<Nb; ++i)
    {
      
      bool in_domain = false;
      
      // We will generate a bead, whose coordinate is in the domain
      std::size_t count = 0;
      while(in_domain==false)
      {
        // generate random vector vn,  -1 < vn(j) < +1
        for (std::size_t j=0; j<3; ++j)
          nv(j) = 2.0*(std::rand() % 1000 ) / 1000. - 1.0;
        
        if (dim==2) nv(2) = 0.0;
        
        // normalize the vector, and let |vn| = 1
        const Real sr = nv.size();
        for (std::size_t j=0; j<3; ++j)   nv(j) /= sr;
        
        // bond (spring) vector: lenght and direction
        for (std::size_t j=0; j<3; ++j)   bv(j) = Ls*nv(j);
        
        // position of the new bead.
        if (i==0)
          pt1 = pt0;
        else
          pt1 += bv;
        // end if-else
        
        // check if the new bead is in the assigned domain.
        for(std::size_t j=0; j<3; ++j)
        {
          if( (pt1(j)<=bbox_min(j))  || (pt1(j)>=bbox_max(j)) )
          {
            in_domain = false;
            pt1 -= bv;
            break;    // if the new bead is outside the domain, break j-loop
          }
          else
            in_domain = true;
          // end if-else
        } // end for j-loop
        
        count++;
        printf("---> test in generate_polymer_chain: bead %lu, count = %lu, vn = (%f,%f,%f)\n",
               i, count, nv(0), nv(1), nv(2) );
        
      } // end while loop
      printf("---> test in generate_polymer_chain: new bead xyz = (%f,%f,%f)\n",pt1(0),pt1(1),pt1(2));
      
      // write out the coordinates
      outfile.setf(std::ios::right);    outfile.setf(std::ios::fixed);
      outfile.precision(o_precision);   outfile.width(o_width);
      outfile << pt1(0) << "  " << pt1(1) << "  " << pt1(2) << "  " << r << "  "<< den << "  \n";
    } // end loop-i
    
    outfile.close();
    printf("---> test in generate_polymer_chain: polymer chain file is created!\n");
  }
  
//  this->comm().barrier();
  return;
}



// ======================================================================
void PolymerChain::print_info() const
{
  START_LOG ("print_info()", "PolymerChain");
  
  
  printf("============================================================================\n");
  printf("*** printing the information of polymer chain: \n");
  printf("============================================================================\n");
  
  printf("There are totally %lu beads and %lu bonds in the polymer chain\n",_n_beads, _n_bonds);
  printf("   %lu bead types\n",_n_bead_types);
  printf("   %lu bond types\n",_n_bond_types);
  
  
  // Masses
  printf("\nMasses:\n");
  for(std::size_t i=0; i<_n_bead_types; ++i) {
    printf("   %lu  %f\n",i, _mass[i]);
  }
  
  // beads
  printf("\nBeads:\n");
  for(std::size_t i=0; i<_n_beads; ++i) {
    printf("   %d %d %d  %f %f %f  %f %f %f\n",
           _beads[i]->id(),_beads[i]->parent_id(),_beads[i]->point_type(),
           _beads[i]->center()(0), _beads[i]->center()(1), _beads[i]->center()(2),
           _beads[i]->orientation()[0], _beads[i]->orientation()[1], _beads[i]->orientation()[2]);
  }
  
  // Bonds
  printf("\nBonds:\n");
  for(std::size_t i=0; i<_n_bonds; ++i) {
    printf("   %lu  %lu %lu %lu\n", i, _bonds[i][0], _bonds[i][1], _bonds[i][2]);
  }
  printf("\n");
  
  
  // Bead info (Note there is no neighbor list info before initialized)
  printf("There are totally %lu beads in the polymer chain:\n",_n_beads);
  for (std::size_t j=0; j<_n_beads; ++j) {
    _beads[j]->print_info();
  }
  printf("======================= end of the polymer information =======================\n\n");
  
  STOP_LOG ("print_info()", "PolymerChain");
}

  
  
// ======================================================================
void PolymerChain::write_polymer_chain(const std::string& filename) const
{
  START_LOG ("write_polymer_chain()", "PolymerChain");
  

  // OFSTEAM
  std::ofstream outfile;
  outfile.open(filename, std::ios_base::out);
  const std::size_t n_beads = _beads.size();
  
  // write out the VTK file
  outfile << "# vtk DataFile Version 4.0\n";
  outfile << "Polymer chain\n";
  outfile << "ASCII\n";
  outfile << "DATASET POLYDATA\n";
  
  // POINT data
  outfile << "POINTS " << n_beads << " float\n";
  for(std::size_t i=0; i<n_beads; ++i)
  {
    for(std::size_t j=0; j<3; ++j){
      outfile << _beads[i]->center()(j) << " ";
    }
    outfile << "\n";
  }
  outfile << "\n";
  
  // LINE(spring) data, which can be visualized by connectivity in Paraview
  const std::size_t Ns = _bonds.size();
  outfile << "LINES " << Ns << " " << Ns*3 << "\n";
  for(std::size_t i=0; i<Ns; ++i)
  {
    outfile << 2 << " " << _bonds[i][1] << " " << _bonds[i][2] << "\n";
  }
  
  // Define the bead types
  outfile << "POINT_DATA " << n_beads << "\n";
  outfile << "SCALARS " << "bead_type "<< "int" << "\n";
  outfile << "LOOKUP_TABLE default \n";
  for(std::size_t i=0; i<n_beads; ++i)
  {
    const int parent_id = _beads[i]->parent_id();
    //const int b_type = (parent_id>0)? parent_id : 1;
    const int b_type = parent_id;
    outfile << b_type << " \n";
  }
  
  outfile.close();

  
  STOP_LOG ("write_polymer_chain()", "PolymerChain");
}
  
  

// ======================================================================
Point PolymerChain::spring_vector(const unsigned int i) const
{
  START_LOG ("spring_vector()", "PolymerChain");
  
  // Check: i cannot exceed the largest number of spring
  libmesh_assert_less(i, this->n_beads()-1);
  
  // Get the two connected beads
  const Point& b0 = _beads[i]->center();
  const Point& b1 = _beads[i+1]->center();
  
  // Compute the spring vector b1 - b0
  const Point b01 = this->bead_vector(b0,b1);
  
  STOP_LOG ("spring_vector()", "PolymerChain");
  return b01;
}

  

// ======================================================================
Point PolymerChain::end_to_end_vector() const
{
  START_LOG ("end_to_end_vector()", "PolymerChain");
  
  // R_end - R_start
  const std::size_t end_id = _beads.size() - 1;
  
  // Check: the polymer at least contains two beads!
  libmesh_assert_less(0, end_id); // end_id > 0 => 1,2,...
  
  // Get the two connected beads
  const Point& b0 = _beads[0]->center();
  const Point& b1 = _beads[end_id]->center();
  
  // Compute the spring vector b1 - b0
  const Point b01 = this->bead_vector(b0,b1);
  
  STOP_LOG ("end_to_end_vector()", "PolymerChain");
  return b01;
}

  
  
// ======================================================================
Point PolymerChain::end_to_end_vector_square() const
{
  START_LOG ("end_to_end_vector_square()", "PolymerChain");
  
  Real val = 0.;
  
  // # of springs; and loop over each spring
  const std::size_t Ns = _beads.size() - 1;
  for(std::size_t m=0; m<Ns; ++m)
  {
    // bond vector r_m
    const Point& b0 = _beads[m]->center();
    const Point& b1 = _beads[m+1]->center();
    const Point bm = this->bead_vector(b0,b1);
    
    for(std::size_t n=0; n<Ns; ++n)
    {
      const Point& b2 = _beads[n]->center();
      const Point& b3 = _beads[n+1]->center();
      const Point bn = this->bead_vector(b2,b3);
      
      val += bm(0)*bn(0) + bm(1)*bn(1) +bm(2)*bn(2);
    } // end for n
  } // end for m
  
  
  STOP_LOG ("end_to_end_vector_square()", "PolymerChain");
  return val;
}
  
  
  
// ======================================================================
Real PolymerChain::compute_chain_length()
{
  START_LOG ("compute_chain_length()", "PolymerChain");
  
  Real len = 0.0;
  
  // # of springs; and loop over each spring
  const std::size_t Ns = _beads.size() - 1;
  for(std::size_t i=0; i<Ns; ++i)
  {
    //
    const Point& b0 = _beads[i]->center();
    const Point& b1 = _beads[i+1]->center();
    
    // Evaluate the bead distance (spring length)
    const Point dpt = this->bead_vector(b0,b1);
    
    len += dpt.size();
  }
  
  STOP_LOG ("compute_chain_length()", "PolymerChain");
  return len;
}
  


// ======================================================================
bool PolymerChain::check_chain(const Real& Ls)
{
  START_LOG ("check_chain()", "PolymerChain");
  
  std::cout << "The function check_chain() has not been implemented yet!" << std::endl;
  //libmesh_error();
  
  bool chain_broken = false;
  
  // # of springs; and loop over each spring
  const std::size_t Ns = _beads.size() - 1;
  for(std::size_t i=0; i<Ns; ++i)
  {
    // Get two connected beads
    const Point& b0 = _beads[i]->center();
    const Point& b1 = _beads[i+1]->center();
    
    // Evaluate the bead distance (spring length)
    const Point dpt = this->bead_vector(b0,b1);
    
    // Check if the length is larger than the maximum length
    if( dpt.size() >= Ls ){
      chain_broken = true;
    }
  }
  
  
  STOP_LOG ("check_chain()", "PolymerChain");
  return chain_broken;
}
  
  
  
// ======================================================================
Point PolymerChain::bead_vector(const Point& bead0,
                                const Point& bead1) const
{
  // Evaluate the bead distance (spring length)
  Point dpt;
  if(_periodic_boundary==NULL) {
    dpt = bead1 - bead0;
  }
  else {
    dpt = _periodic_boundary->point_vector(bead0,bead1);
  }
  
  // return
  return dpt;
}
  

} // end of the namespace
