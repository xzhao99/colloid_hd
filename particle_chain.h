//
//  particle_chain.h
//  
//
//  Created by Xujun Zhao on 1/21/16.
//
//

#ifndef particle_chain_h
#define particle_chain_h

#include <stdio.h>

#include "libmesh/reference_counted_object.h"
#include "libmesh/parallel_object.h"

#include "polymer_chain.h"
#include "rigid_particle.h"



namespace libMesh
{
  
  /*
   * This class defines a particle-chain object, in which particles
   * are respresented by finite element mesh, and the chains are described 
   * by bead-spring model
   */
  
  
class ParticleChain :  public ReferenceCountedObject<ParticleChain>,
                       public ParallelObject
{
public:
  
  // Constructor
  ParticleChain(const std::size_t n_particles,
                const std::size_t n_chains,
                const Parallel::Communicator &comm_in);
  
  
  // Destructor
  ~ParticleChain();
  
  
  /*
   * Read the data of chain from local file, whose data structure is:
   * Nb
   * # type x0 y0 z0 a0 b0 c0 theta0
   * # type x1 y1 z1 a1 b1 c1 theta1
   * ......
   *
   * particle_type_id: type id to identify the point is a particle
   */
  void read_data(const std::string& filename,
                 const std::string& vmesh_file,
                 const std::string& smesh_file,
                 const std::string& mesh_type,
                 const std::size_t  particle_type_id = 1);
  
  
  /*
   * read partilces
   */
  void read_particles(const std::string& filename,
                      const std::string& vmesh_file,
                      const std::string& smesh_file,
                      const std::string& mesh_type,
                      const std::size_t  particle_type_id = 1);
  
  
  /*
   * read chains
   */
  void read_chains(const std::string& filename,
                   const std::size_t  particle_type_id = 1);
  
  
private:
  // number of particles
  std::size_t _n_particles;
  
  
  // number of particles
  std::size_t _n_chains;
  
  /*
   * If we use std::vector<PolymerChain> or std::vector<RigidParticle>,
   * vector's resize() function doesn't know how much memory to allocate
   * for the objects. Therefore, we use pointers instead.
   */
  
  // particles
  std::vector<RigidParticle*> _particles;

  // polymer chains (can be more than one chain)
  std::vector<PolymerChain*> _chains;
  
};  // end of class
  
}  // end of namespace








#endif /* particle_chain_h */
