//
//  test_validation_02.h
//  
//
//  Created by Xujun Zhao on 5/4/16.
//
//

#ifndef test_validation_02_h
#define test_validation_02_h


#include <fstream>

#include "libmesh/getpot.h"
#include "libmesh/mesh.h"
#include "libmesh/serial_mesh.h"
#include "libmesh/mesh_generation.h"
#include "libmesh/mesh_modification.h"
#include "libmesh/mesh_refinement.h"

#include "libmesh/gmv_io.h"
#include "libmesh/exodusII_io.h"

#include "libmesh/dof_map.h"
#include "libmesh/linear_solver.h"
#include "libmesh/equation_systems.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/periodic_boundary.h"
#include "libmesh/sparse_matrix.h"
#include "libmesh/numeric_vector.h"


// include user defined classes or functions
#include "point_particle.h"
#include "rigid_particle.h"
#include "particle_mesh.h"
#include "point_mesh.h"
#include "force_field.h"
#include "pm_linear_implicit_system.h"
#include "mesh_spring_network.h"
#include "pm_toolbox.h"
#include "pm_periodic_boundary.h"
#include "analytical_solution.h"
//#include "brownian_system.h"




/*
 * This example simulates a single particle in a 3D cylider
 * subjected to a force along the axial (z) direction.
 */



int test_validation_02(const Parallel::Communicator &comm_in)
{
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### running test_validation_02\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Define constants: We use um as the length unit
   kB = 1.380662E-23(J/K) = 1.380662E-23(N*m/K) = 1.380662E-17 (N*um/K)
   T  = 297 K
   ===> kB*T = 4.1E-15 (N*um)
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  const Real PI     = libMesh::pi;
  const Real kBT    = 4.1E-15;     //
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Parse the input file and read in parameters from the input file
   (1) restart mode
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  GetPot input_file("rigid_particle_control.in");
  const bool  restart       = input_file("restart", false);
  std::size_t restart_step  = input_file("restart_step", 0);
  std::size_t random_seed   = input_file("random_seed",111);
  Real        restart_time  = input_file("restart_time", 0.0);
  if(restart) // update the seed for restart mode
  {
    random_seed++;
  }
  else        // set the restart_step as zero
  {
    restart_step = 0;
    restart_time = 0.0;
  }
  
  
  // (2) Stokes solver parameters
  const int max_linear_iterations = input_file("max_linear_iterations", 100);
  const Real linear_solver_rtol   = input_file("linear_solver_rtol", 1E-6);
  const Real linear_solver_atol   = input_file("linear_solver_atol", 1E-6);
  bool user_defined_pc            = input_file("user_defined_pc", true);
  const bool schur_user_ksp       = input_file("schur_user_ksp", false);
  const Real schur_user_ksp_rtol  = input_file("schur_user_ksp_rtol", 1E-6);
  const Real schur_user_ksp_atol  = input_file("schur_user_ksp_atol", 1E-6);
  const std::string schur_pc_type = input_file("schur_pc_type", "SMp");
  const std::string stokes_solver_type = input_file("stokes_solver", "superLU_dist");
  StokesSolverType solver_type;
  if(stokes_solver_type=="superLU_dist") {
    solver_type = superLU_dist;
    user_defined_pc = false;
  }
  else if(stokes_solver_type=="field_split") {
    solver_type = field_split;
    user_defined_pc = true;
  }
  else {
    solver_type = user_define;
  }
  
  
  // (3) Physical parameters
  const Real viscosity            = input_file("viscosity", 1.0); // viscosity N*s/um^2
  const Real ev                   = input_file("ev", 1E-3); // excluded volume parameter
  const Real alpha                = input_file("alpha", 0.1);

  
  // (4) Print out the information of the Stokes solver
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The Stokes solver info:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "Stokes solver type = " << stokes_solver_type << std::endl;
  if (stokes_solver_type=="field_split")
  {
    std::cout << "FieldSplit Schur Complement Reduction Solver\n";
    std::cout << "schur_pc_type = " << schur_pc_type << std::endl;
    if(schur_user_ksp)
    {
      std::cout<<"user defined KSP is used for Schur Complement!"<< std::endl;
      std::cout<<"KSP rel tolerance for Schur Complement solver is = " << schur_user_ksp_rtol <<"\n";
      std::cout<<"KSP abs tolerance for Schur Complement solver is = " << schur_user_ksp_atol <<"\n";
    }
  }
  std::cout << "\n\n";
  
  
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The physical parameters in the simulation:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "   viscosity                          mu  = " << viscosity <<" (N*s/um^2)\n";
  std::cout << "   Excluded volume parameter          ev  = " << ev << " (um^3)\n";
  std::cout << "   the smoothing parameter in GGEM alpha  = " << alpha << "\n";
  std::cout << "\n\n";
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * Create a mesh, distributed across the default MPI communicator.
   * We build a mesh with Quad9(8) elements for 2D and HEX27(20) element for 3D
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### Create finite element mesh of the fluid:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  SerialMesh mesh(comm_in);   //Mesh mesh(comm_in);
  mesh.read("Cylinder_R10_H300_Mesh_F4_Sweep.e"); // read the fluid mesh.
  //mesh.read("Cylinder_R10_H200_Mesh_AppSize_1.e"); // read the fluid mesh=> 1.8M dofs.
  mesh.all_second_order();
  /*  Magnify the mesh of cylinder (originally R=10, H=100)  */
  std::vector<Real> mag_factor(3);
  mag_factor[0] = 1.0/5.0; mag_factor[1] = mag_factor[0]; mag_factor[2] = mag_factor[0];
  PMToolBox::magnify_serial_mesh(mesh,mag_factor);
  mesh.print_info();
  std::cout << "\n\n";
  
  // geometry parameters
  const unsigned int dim = mesh.mesh_dimension();
  std::vector<Real> fluid_mesh_size = PMToolBox::mesh_size(mesh);
  const Real min_mesh_size = fluid_mesh_size[0];
  const Real max_mesh_size = fluid_mesh_size[1];
  const Real R_cylinder = 10.*mag_factor[0];  // R = 10
  const Real H_cylinder = 299.5*mag_factor[0]; // H = 300
  const Real ZA = -H_cylinder/2.0, ZB = +H_cylinder/2.0;
  const Real YA = -R_cylinder, YB = +R_cylinder;
  const Real XA = -R_cylinder, XB = +R_cylinder;
  std::cout << "XA = " << XA << ", XB = " << XB << std::endl;
  std::cout << "YA = " << YA << ", YB = " << YB << std::endl;
  std::cout << "ZA = " << ZA << ", ZB = " << ZB << std::endl;
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Build boundary info: set bottom side ID = 0, and top side ID = 5.
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  MeshBase::const_element_iterator       el     = mesh.active_local_elements_begin();
  const MeshBase::const_element_iterator end_el = mesh.active_local_elements_end();
  const Real geom_tol = 1E-6;
  for ( ; el != end_el; ++el)
  {
    // Store a pointer to the element we are currently working on.
    const Elem* elem = *el;
    
    // Loop over each side of an element, and find the boundary edge
    for (unsigned int s=0; s<elem->n_sides(); s++)
    {
      if (elem->neighbor(s) == NULL)
      {
        UniquePtr<Elem> side(elem->build_side(s)); // side elem with the same order as the bulk elem
        unsigned int bottom_side_node = 0, top_side_node = 0, wall_side_node = 0;
        
        // loop over each node on this side
        for (unsigned int ns=0; ns<side->n_nodes(); ns++)
        {
          const Point& node_pt = side->point(ns);
          
          if( std::abs(node_pt(2)-ZA) < geom_tol ){
            bottom_side_node++;  // bottom side node
          }
          else if( std::abs(node_pt(2)-ZB) < geom_tol ){
            top_side_node++; // top side node
          }
          else {
            wall_side_node++;  // all other sides(channel walls)
          } // end if-else
        } // end for ns-loop
        
        // add edge id
        if( bottom_side_node>=6 ){
          mesh.get_boundary_info().add_side(elem,s,0);  // bottom side edge
        }
        if( top_side_node>=6 ){
          mesh.get_boundary_info().add_side(elem,s,5);  // top side edge
        }
        if( wall_side_node>=6 ){
          mesh.get_boundary_info().add_side(elem,s,1);  // wall edge
        }
        
        // - - - - - - - - - - - - - - - - - - TEST output - - - - - - - - - - - - - - - - - -
//        printf("bottom_side_node = %u, top_side_node = %u, wall_side_node = %u\n",
//               bottom_side_node, top_side_node, wall_side_node);
        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      } // end if
      
    } // end for s-loop
    
  } // end for el-loop
//  mesh.get_boundary_info().print_info();
//  return 0;
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Test the ParticleMesh/PointMesh class
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### Create particle-mesh and point-mesh system:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  // Names of surface and volume mesh files for the particle
  const std::string vmesh = "volume_mesh_medium.e"; // coarse/medium/fine (sphere)
  const std::string smesh = "surface_mesh_sphere.e";
  //  const std::string vmesh = "cylinder_3D_vol_mesh_level_7.e"; // coarse/medium/fine (cylinder)
  //  const std::string smesh = "surface_mesh_cylinder.e";
  const std::string mesh_type = "surface_mesh"; // or "volume_mesh";
  
  // add particle-mesh periodic boundary in the x-direction
  const Point bbox_pmin(XA, YA, ZA);
  const Point bbox_pmax(XB, YB, ZB);
  std::vector<bool> periodic_direction(dim,false);
//  periodic_direction[0] = true; // periodic boundary in x-direction
//  periodic_direction[1] = true; // periodic boundary in y-direction
//  periodic_direction[2] = true; // periodic boundary in z-direction
  PMPeriodicBoundary pm_periodicity(bbox_pmin, bbox_pmax, periodic_direction);
  
  // Read the particle data and surface mesh
  const Real search_radius_p = 4.0/alpha;
  const Real search_radius_e = 0.5*max_mesh_size + search_radius_p;
  ParticleMesh<3> particle_mesh(mesh,search_radius_p,search_radius_e);
  particle_mesh.add_periodic_boundary(pm_periodicity);
  std::ostringstream pfilename;
  if(restart)
  {
    // read data output from the assigned step for restart mode.
  }
  else
  {
    // read the initial input data
    pfilename << "rigid_particle_data.in";
    particle_mesh.read_particles_data(pfilename.str(),vmesh,smesh,mesh_type);
  }
  pfilename.str(""); pfilename.clear();
  comm_in.barrier();
  
  
  // Re-initialize ParticleMesh and PointMesh.
  particle_mesh.reinit();
  const std::vector<Real> hsize_solid = particle_mesh.mesh_size();// mesh size of solid
  if(comm_in.rank()==0){
    printf("---> mesh size for solid: hmin = %f, hmax = %f\n\n",hsize_solid[0],hsize_solid[1] );
    printf("---> mesh size for fluid: hmin = %f, hmax = %f\n\n",min_mesh_size,max_mesh_size);
    printf("     smoothing parameter ksi = 1/(0.75*hsmax) = %f\n\n", 1.0/(0.75*hsize_solid[1]));
  }
  
  
  /// ------------------------- TEST ParticleMesh function ---------------------------
  for (std::size_t i=0; i<particle_mesh.num_particles(); ++i)
  {
    Real vol  = particle_mesh.particles()[i]->compute_volume();
    Real area = particle_mesh.particles()[i]->compute_area();
    Point pc_i = particle_mesh.particles()[i]->compute_centroid(mesh_type);
    const std::vector<Real> hs = particle_mesh.particles()[i]->mesh_size();
    //particle_mesh.particles()[i]->compute_surface_normal(mesh_type);
    
    if(comm_in.rank()==0)
    {
      printf("------> Volume of the %lu-th particle is %f, and area = %f\n",i,vol,area);
      const Real Ri = particle_mesh.particles()[i]->radius();
      const Real vol_th = 4./3.*PI*Ri*Ri*Ri, area_th = 4.*PI*Ri*Ri;
      printf("        The theoretical value of V is %f, rel error = %f\n",
             vol_th,std::abs(vol-vol_th)/vol_th);
      printf("        The theoretical value of A is %f, rel error = %f\n",
             area_th,std::abs(area-area_th)/area_th);
      printf("        The centroid = (%f,%f,%f)\n\n", pc_i(0),pc_i(1),pc_i(2));
      printf("        hmin = %f, hmax = %f\n\n", hs[0],hs[1] );
    }
  }
  /// --------------------------------------------------------------------------------
  
  
  // Construct the PointMesh class from the ParticleMesh
  PointMesh<3> point_mesh(particle_mesh,search_radius_p,search_radius_e);
  point_mesh.reinit();
  
  // print out info
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The particle-mesh and point-mesh info:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "Total number of particles: " << particle_mesh.num_particles() << "\n";
  std::cout << "Total number of points: " << particle_mesh.num_mesh_points() << "\n";
  for(std::size_t i=0; i<particle_mesh.num_particles(); ++i)
  {
    const std::size_t n_surf_elem = particle_mesh.particles()[i]->num_mesh_elem();
    const std::size_t n_surf_node = particle_mesh.particles()[i]->num_mesh_nodes();
    std::cout << "  Particle "<<i<<": center " << particle_mesh.particles()[i]->center() <<"\n";
    std::cout << "         " << n_surf_elem << " elements for the particle's mesh" << "\n";
    std::cout << "         " << n_surf_node << " nodes  for the particle's mesh" << "\n";
  }
  
  std::cout <<"search_radius_p = "<<search_radius_p <<", search_radius_e = "<<search_radius_e<<"\n";
  std::cout <<"Periodic BC in x-y-z directions: ";
  for (std::size_t i=0; i<dim; ++i)
  {
    if( periodic_direction[i] ){
      std::cout<<"  TRUE";
    }
    else{
      std::cout<<"  FALSE";
    }
  }
  std::cout << "\n\n\n";
  //if(comm_in.rank()==0) point_mesh.print_point_info();
//  return 0;
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Create an equation systems and ParticleMeshLinearImplicitSystem,
   and add variables: velocity (u, v, w) and pressure p. To satisfy LBB condition,
   (u, v, w):  second-order approximation
   p :         first-order basis
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  EquationSystems stokes_es (mesh);
  PMLinearImplicitSystem& pm_system = stokes_es.add_system<PMLinearImplicitSystem> ("Stokes");
  unsigned int u_var = 0, v_var = 0, w_var = 0;
  u_var = pm_system.add_variable ("u", SECOND);
  v_var = pm_system.add_variable ("v", SECOND);
  if(dim==3)  w_var  = pm_system.add_variable ("w", SECOND);
  const unsigned int p_var = pm_system.add_variable ("p", FIRST);
  
  /* attach the particle-mesh system */
  pm_system.attach_particle_mesh(&particle_mesh);
  pm_system.attach_point_mesh(&point_mesh);
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Add periodic boundary conditions for the system.
   For the side number of a box, refer to libMesh::Hex27::build_side()
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  DofMap& dof_map = pm_system.get_dof_map();
  
  /*** set PBC in x-direction ***/
  if (periodic_direction[0])
  {
    PeriodicBoundary pbcx(RealVectorValue(XB-XA, 0., 0.));
    pbcx.set_variable(u_var);
    pbcx.set_variable(v_var);
    if(dim==3) pbcx.set_variable(w_var);
    //    pbcx.set_variable(p_var); //*** NOT include p!
    
    // boundary number
    if(dim==2)
    {
      pbcx.myboundary = 3;
      pbcx.pairedboundary = 1;
    }
    else if(dim==3)
    {
      pbcx.myboundary = 4;
      pbcx.pairedboundary = 2;
    } // end if
    
    dof_map.add_periodic_boundary(pbcx);
    
    // check
    if (search_radius_p>=(XB-XA)/2. && comm_in.rank()==0)
    {
      printf("\n\n");
      printf("****************************** warning: ********************************\n");
      printf("**** The search radius is larger than the domain length in x direction! \n");
      printf("**** search radius = %f, domain size Lx = %f\n",search_radius_p,(XB-XA)/2.);
      printf("************************************************************************\n\n\n");
    }
  }
  
  /*** set PBC in y-direction ***/
  if (periodic_direction[1])
  {
    PeriodicBoundary pbcy(RealVectorValue(0., YB-YA, 0.));
    pbcy.set_variable(u_var);
    pbcy.set_variable(v_var);
    if(dim==3) pbcy.set_variable(w_var);
    //    pbcy.set_variable(p_var); //*** NOT include p!
    
    if(dim==2)
    {
      pbcy.myboundary = 0;
      pbcy.pairedboundary = 2;
    }
    else if(dim==3)
    {
      pbcy.myboundary = 1;
      pbcy.pairedboundary = 3;
    } // end if
    
    dof_map.add_periodic_boundary(pbcy);
    
    // check
    if (search_radius_p>=(YB-YA)/2. && comm_in.rank()==0)
    {
      printf("\n\n");
      printf("****************************** warning: ********************************\n");
      printf("**** The search radius is larger than the domain length in y direction!\n");
      printf("**** search radius = %f, domain size Ly = %f\n",search_radius_p,(YB-YA)/2.);
      printf("************************************************************************\n\n\n");
    }
  }
  
  /*** set PBC in y-direction ***/
  if (periodic_direction[2])
  {
    PeriodicBoundary pbcz(RealVectorValue(0., 0., ZB-ZA));
    pbcz.set_variable(u_var);
    pbcz.set_variable(v_var);
    if(dim==3) pbcz.set_variable(w_var);
    //pbcz.set_variable(p_var); //*** NOT include p!
    
    if(dim==3)
    {
      pbcz.myboundary = 0;
      pbcz.pairedboundary = 5;
    } // end if
    
    dof_map.add_periodic_boundary(pbcz);
    
    // check
    if (search_radius_p>=(ZB-ZA)/2. && comm_in.rank()==0)
    {
      printf("\n\n");
      printf("****************************** warning: ********************************\n");
      printf("**** The search radius is larger than the domain length in z direction!\n");
      printf("**** search radius = %f, domain size Lz = %f\n",search_radius_p,(ZB-ZA)/2.);
      printf("************************************************************************\n\n\n");
    }
  }
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Initialize the Preconditioning matrix for saddle point problems if required.
   Initialize the equation system and zero the preconditioning matrix
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if( user_defined_pc ) pm_system.add_matrix("Preconditioner");
  
  /* Initialize the data structures for the equation system. */
  stokes_es.init ();
  
  // zero the PC matrix, which MUST be done after es.init()
  if( user_defined_pc ) pm_system.get_matrix("Preconditioner").zero();
  
  std::cout<<"###Equation systems are initialized:\n"<<std::endl;

  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   set parameters of equations systems
   * NOTE: some of these parameters are not used if we use non-dimensional formulation
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  stokes_es.parameters.set<unsigned int>("linear solver maximum iterations") = max_linear_iterations;
  stokes_es.parameters.set<Real>   ("linear solver rtol") = linear_solver_rtol;
  stokes_es.parameters.set<Real>   ("linear solver atol") = linear_solver_atol;
  stokes_es.parameters.set<bool>      ("user_defined_pc") = user_defined_pc;
  stokes_es.parameters.set<bool>       ("schur_user_ksp") = schur_user_ksp;
  stokes_es.parameters.set<Real>  ("schur_user_ksp_rtol") = schur_user_ksp_rtol;
  stokes_es.parameters.set<Real>  ("schur_user_ksp_atol") = schur_user_ksp_atol;
  stokes_es.parameters.set<std::string> ("schur_pc_type") = schur_pc_type;
  stokes_es.parameters.set<StokesSolverType> ("solver_type") = solver_type;
  
//  stokes_es.parameters.set<Real>        ("XA_boundary") = XA;
//  stokes_es.parameters.set<Real>        ("XB_boundary") = XB;
//  stokes_es.parameters.set<Real>        ("YA_boundary") = YA;
//  stokes_es.parameters.set<Real>        ("YB_boundary") = YB;
//  stokes_es.parameters.set<Real>        ("ZA_boundary") = ZA;
//  stokes_es.parameters.set<Real>        ("ZB_boundary") = ZB;
  stokes_es.parameters.set<Real>              ("alpha") = alpha;
  stokes_es.parameters.set<Real>    ("fluid mesh size") = min_mesh_size;
  stokes_es.parameters.set<Real>    ("solid mesh size") = hsize_solid[1];
  
  stokes_es.parameters.set<Real>         ("viscosity")  = viscosity;
  stokes_es.parameters.set<Real>       ("viscosity_0")  = 1./(6.*PI);
  stokes_es.parameters.set<Real>               ("br0")  = 1.0;
  stokes_es.parameters.set<Real>               ("kBT")  = kBT;
  stokes_es.parameters.set<Real>                ("ev")  = ev;
  stokes_es.parameters.set<std::string> ("particle_type")  = "rigid_particle";
  stokes_es.parameters.set<std::string> ("particle_mesh_type")  = mesh_type;  // used in ForceField
  
  /* Print information about the mesh and system to the screen. */
  std::cout <<"  Stokes System has: "<< mesh.n_elem()<<" elements,\n"
            <<"              "<< mesh.n_nodes()<<" nodes,\n"
            <<"              "<< stokes_es.n_active_dofs()<<" active degrees of freedom.\n"
            <<"              "<< particle_mesh.num_particles()<<" particles.\n" << std::endl;
  stokes_es.print_info();
  return 0;
  /* ------------------------------------------------------------------------------------*/
  
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   build MeshSpringNetwork according to the particle's mesh, which will be used to
   apply the rigid-body constraint force.
   
   Note: if the particles use different meshes, we need to build them for each of particles!
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  MeshBase& p_mesh = particle_mesh.particles()[0]->mesh();
  const Point center0 = particle_mesh.particles()[0]->center();
  MeshSpringNetwork mesh_spring_network(p_mesh,pm_periodicity);
  mesh_spring_network.build_spring_network(center0);
  //if(comm_in.rank()==0) mesh_spring_network.print_info();
  
  
  // attach the SAME mesh spring network for each particle,
  // because we use the same mesh for all the particles here!
  for(std::size_t i=0; i<particle_mesh.num_particles(); ++i)
  {
    particle_mesh.particles()[i]->attach_mesh_spring_network(&mesh_spring_network);
  }
  //return 0;
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Init the force field and attach it to the PMLinearImplicitSystem pm_system
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ForceField force_field(pm_system);
  pm_system.attach_force_field(&force_field);
  //return 0;
  
  
  
  /* ------------------------------------------------------------------------------------*/
  /* ---------------------------------- TEST PROGRAM ----------------------------------- */
  /* ------------------------------------------------------------------------------------*/
  std::string msg = "--->TEST reinit particle mesh:";
  PMToolBox::output_message(msg,comm_in);
  pm_system.reinit_system();
//  if(comm_in.rank()==0)
//  {
//    point_mesh.print_point_info();
//    //point_mesh.print_elem_neighbor_list();
//  }

  msg = "--->TEST Stokes solver with point forces:";
  PMToolBox::output_message(msg,comm_in);
  bool re_init_stokes = true;
  pm_system.solve_stokes("undisturbed",re_init_stokes);
  ExodusII_IO(mesh).write_equation_systems("output_validation_test02_pm_system0.e",stokes_es);

  re_init_stokes = false;
  pm_system.solve_stokes("disturbed",re_init_stokes);

  // print out the velocity
  std::vector<Real> vel1 = pm_system.compute_point_velocity("disturbed");
  const unsigned int NP0 = point_mesh.num_particles();
  
  Real u_max, u_min, u_avg = 0.;
  if(comm_in.rank()==0)
  {
    for(unsigned int j=0; j<NP0;++j)
    {
      Point vtest1(dim);
      for(std::size_t k=0; k<dim;++k){
        vtest1(k) = vel1[j*dim+k];
      }
      if(j==0)
      {
        u_min = vtest1.size();
        u_max = vtest1.size();
      }
      else
      {
        u_min = (vtest1.size() < u_min) ? vtest1.size() : u_min;
        u_max = (vtest1.size() > u_max) ? vtest1.size() : u_max;
      }
      u_avg += vtest1.size();
//      printf("--->test: velocity on the %u-th point:\n",j);
//      printf("        U1 = (%f,%f,%f)\n\n", vtest1[0],vtest1[1],vtest1[2]);
    }
    u_avg /= Real(NP0);
    printf("--->test: vmax = %f, vmin = %f, vavg = %f.\n",u_max,u_min,u_avg);
  }
  //force_field.modify_force_field(vel1); // test
  
  pm_system.add_local_solution();
  ExodusII_IO(mesh).write_equation_systems("output_validation_test02_pm_system1.e",stokes_es);
  particle_mesh.write_particle_mesh("output_validation_test02_particles_mesh.e");
  
  
  // compare with analytical solution
  AnalyticalSolution analytical_solution(pm_system);
  const Real particle_radius = particle_mesh.particles()[0]->radius();
  const Real r_r0 = particle_radius/R_cylinder;
  const Real k_bohlin   = analytical_solution.correction_factor_bohlin(r_r0);
  const Real k_haberman = analytical_solution.correction_factor_haberman(r_r0);
  
  // F1 = F/(6*PI*mu*R) = 4/3*PI*R^2 * rho = 1/3*rho*S
  Real area = particle_mesh.particles()[0]->compute_area();
//  const Real F1_a = 1.0/3.0*area*0.1;
  const Real F1 = 4./3.*PI*particle_radius*particle_radius*0.1;
  
  if(comm_in.rank()==0)
  {
    const Real k_num_avg = F1/u_avg;
    const Real k_num_min = F1/u_min;
    const Real k_num_max = F1/u_max;
    printf("--->test: correction factor when r/r0 = %f\n",r_r0);
//    printf("    F1_theo = %f, F1_num = %f, kn_avg = %f, kn_min = %f, kn_max = %f.\n",
//           F1, F1_a, F1_a/u_avg, F1_a/u_min, F1_a/u_max);
    printf("    kb = %f, kh = %f, kn_avg = %f, kn_min = %f, kn_max = %f.\n",
           k_bohlin,k_haberman,k_num_avg,k_num_min,k_num_max);
    printf("    k_error_b_avg = %f, k_error_b_min = %f, k_error_b_max = %f.\n",
           std::abs(k_num_avg-k_bohlin)/k_bohlin,
           std::abs(k_num_min-k_bohlin)/k_bohlin,
           std::abs(k_num_max-k_bohlin)/k_bohlin);
    printf("    k_error_h_avg = %f, k_error_h_min = %f, k_error_h_max = %f.\n",
           std::abs(k_num_avg-k_haberman)/k_haberman,
           std::abs(k_num_min-k_haberman)/k_haberman,
           std::abs(k_num_max-k_haberman)/k_haberman);
  }
  
  return 0;
}


/*
---> Fluid mesh: cylinder R10_H300_F5;  Particle mesh: coarse
 
*/
#endif /* test_validation_02_h */
