//
//  force_field.h
//  
//
//  Created by Xujun Zhao on 6/29/15.
//
//

#ifndef ____force_field__
#define ____force_field__

#include <stdio.h>


#include "pm_linear_implicit_system.h"
#include "elasticity_system.h"
#include "force_field_base.h"

namespace libMesh
{

  /*
   * The class is designed for computing the force field
   * of the ParticleMesh system, including
   * -- spring force
   * -- excluded volume force
   * -- gravity
   * -- other user defined forces
   */
  
  
class ForceField : public ForceFieldBase
{
public:
  // Constructor for a system with elastic structures
  ForceField(PMLinearImplicitSystem& pm_sys,
             ElasticitySystem& el_sys);

  
  // Constructor for a system with point particles
  ForceField(PMLinearImplicitSystem& pm_sys);
  
  
  // Destructor
  ~ForceField();
  
  
  typedef ForceFieldBase Parent;
  
  
  /* 
   * User-defined function for reinitializing the force field
   * at each time step
   */
  virtual void reinit_force_field();
  
  
  
  /*
   * Re-init the force vector of each point (set to zeros)
   * This is required at the beginning of each time step when
   * re-compute the forces on each point.
   */
  void reinit_point_force_zeros();
  
  
  
  /*
   * User-defined function for reinitializing the force field
   * of a DNA(polymer) chain at each time step
   */
  void compute_force_field_dna();
  
  
  /*
   * User-defined function that incorporates:
   * (a) excluded volume force for particles
   * (b) the friction force between different objects.
   */
  void modify_force_field(const std::vector<Real>& vel_beads);
  
  
  /*
   * compute the constraint forces for the i-th rigid particle
   * This is achieved by applying large stiff spring forces.
   * k0 is a large number for the stiffness constant.
   */
  void rigid_constraint_force(const std::size_t& i, // the i-th particle
                              const Real& k0,
                              std::vector<Point>& nodal_force);
  
  
  
  /*
   * Compute the Spring Force on a given particle. Assume the chain 
   * are connected by Worm-like springs (WLS).
   * The spring force only occurs between two connected beads
   */
  void compute_spring_force_wls(const std::size_t  p_id,
                                std::vector<Real>& pforce ) const;
  
  
  /*
   * Compute the Spring Force on a given particle. Assume the chain
   * are connected by FENE springs.
   * The spring force only occurs between two connected beads
   */
  void compute_spring_force_fene(const std::size_t  p_id,
                                 std::vector<Real>& pforce ) const;
  
  
  /*
   * Compute the Excluded volume Force on a given particle
   * This force occurs between the particle and all its neighbors,
   * which are non-bonded interactions.
   */
  void compute_excluded_volume_force(const std::size_t  p_id,
                                     std::vector<Real>& pforce ) const;
  
  
  /*
   * Compute the particle-wall Force on a given particle
   * This force occurs between the particle and wall, but
   * does NOT in the periodic boundary direction without walls!
   * 
   * The current algorithm only consider the parallel walls with simple geometries.
   */
  void compute_particle_wall_force(const std::size_t  p_id,
                                   std::vector<Real>& pforce ) const;
  
  
  // Correct the bead position if it moves out of the wall
  void check_wall(const std::size_t p_id);
  void check_walls();
  
  

  /*
   * Return a constant force vector.
   * This is a general case of the above function gravity_force()
   */
  void constant_force(const std::size_t  p_id,
                      std::vector<Real>& pforce ) const;
  
  
private:
  
  // the particle-mesh (linear implicit) system
  PMLinearImplicitSystem* _pm_system;
  
  // The elastic system for solids
  // Use pointer instead of const Ref. to avoid explicit initialization!
  ElasticitySystem* _elastic_system;
  //const ElasticitySystem& _elastic_system;
  
  // system dimension
  unsigned int _dim;
  
  // Coefficients used to compute the non-dimensional forces
  Real _bead_r;
  Real _Ss2;
  Real _bk;
  Real _Nks;
  
};  // end of class

  
  
} // end of namespace
#endif /* defined(____force_field__) */
