//
//  test_scalability_01.h
//  
//
//  Created by Xujun Zhao on 5/23/16.
//
//

#ifndef test_scalability_01_h
#define test_scalability_01_h





#include <fstream>

#include "libmesh/getpot.h"
#include "libmesh/mesh.h"
#include "libmesh/serial_mesh.h"
#include "libmesh/mesh_generation.h"
#include "libmesh/mesh_modification.h"
#include "libmesh/mesh_refinement.h"

#include "libmesh/gmv_io.h"
#include "libmesh/exodusII_io.h"

#include "libmesh/dof_map.h"
#include "libmesh/linear_solver.h"
#include "libmesh/equation_systems.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/periodic_boundary.h"
#include "libmesh/sparse_matrix.h"
#include "libmesh/numeric_vector.h"


// include user defined classes or functions
#include "point_particle.h"
#include "particle_mesh.h"
#include "point_mesh.h"
#include "force_field.h"
#include "pm_linear_implicit_system.h"
#include "brownian_system.h"
#include "pm_periodic_boundary.h"
#include "chebyshev.h"
#include "pm_toolbox.h"
#include "polymer_chain.h"
#include "random_generator.h"
#include "stokes_solver.h"
#include "ggem_system.h"



/*
 * This example will perform a scalability test of the
 * GGEM/FEM solver.
 *
 * We fix total number of beads, and change the CPU cores.
 *
 * The analytical solution is given by a Green's function
 * and the exact values at the boundaries will be extracted
 * and used as the boundary conditions in the GGEM solver
 */



/*
 * Parametes required in this example:
 * alpha
 * ksi = std::sqrt(PI)/(3.*a) where a = 1
 * muc = 1.0/(6*PI)
 * dim = 3;
 */



int test_scalability_01(const Parallel::Communicator &comm_in)
{
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### running test_scalability_01\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Define constants:
   kB = 1.380662E-23(J/K) = 1.380662E-23(N*m/K) = 1.380662E-17 (N*um/K)
   T  = 297 K
   ===> kBT = 4.1E-15 (N*um)
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  const Real PI     = libMesh::pi;
  const Real kBT    = 4.1E-15;     //
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Parse the input file and read in parameters from the input file
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  GetPot input_file("polymer_control.in");
  const bool  restart       = input_file("restart", false);
  std::size_t restart_step  = input_file("restart_step", 0);
  std::size_t random_seed   = input_file("random_seed",111);
  Real        restart_time  = input_file("restart_time", 0.0);
  if(restart) // update the seed for restart mode
  {
    random_seed++;
  }
  else        // set the restart_step as zero
  {
    restart_step = 0;
    restart_time = 0.0;
  }
  
  
  // (1) Stokes solver parameters
  const int max_linear_iterations = input_file("max_linear_iterations", 100);
  const Real linear_solver_rtol   = input_file("linear_solver_rtol", 1E-6);
  const Real linear_solver_atol   = input_file("linear_solver_atol", 1E-6);
  bool user_defined_pc            = input_file("user_defined_pc", true);
  const bool schur_user_ksp       = input_file("schur_user_ksp", false);
  const Real schur_user_ksp_rtol  = input_file("schur_user_ksp_rtol", 1E-6);
  const Real schur_user_ksp_atol  = input_file("schur_user_ksp_atol", 1E-6);
  const std::string schur_pc_type = input_file("schur_pc_type", "SMp");
  const std::string stokes_solver_type = input_file("stokes_solver", "superLU_dist");
  StokesSolverType solver_type;
  if(stokes_solver_type=="superLU_dist") {
    solver_type = superLU_dist;
    user_defined_pc = false;
  }
  else if(stokes_solver_type=="field_split") {
    solver_type = field_split;
    user_defined_pc = true;
  }
  else {
    solver_type = user_define;
  }
  
  
  // (2) Mesh & Geometry parameters
  const unsigned int dim          = input_file("dimension", 3);
  const Real XA                   = input_file("XA", -1.);
  const Real XB                   = input_file("XB", +1.);
  const Real YA                   = input_file("YA", -1.);
  const Real YB                   = input_file("YB", +1.);
  Real ZA                         = input_file("ZA", -1.);
  Real ZB                         = input_file("ZB", +1.);
  if (dim==2){  ZA = 0.;  ZB = 0.;  }
  const unsigned int nx_mesh      = input_file("nx_mesh", 20);
  const unsigned int ny_mesh      = input_file("ny_mesh", 10);
  const unsigned int nz_mesh      = input_file("nz_mesh", 10);
  Real alpha                = input_file("alpha", 0.1);
  
  
  // (3) Physical parameters of polymer and fluids
  const Real viscosity            = input_file("viscosity", 1.0); // viscosity (cP = N*s/um^2)
  const unsigned int Ns           = input_file("Ns", 20);   // total # of springs
  const Real bk                   = input_file("bk", 1E-6); // Kuhn length (um)
  const Real Nks                  = input_file("Nks",1E-6); // # of Kuhn length per spring
  const Real Rb                   = input_file("radius", 0.10); // radius of the bead (um)
  const Real ev                   = input_file("ev", 1E-3); // excluded volume parameter
  
  
  // (4) Compute other parameters using the input
  Real min_mesh_size, max_mesh_size;
  const Real meshsize_x   = (XB - XA)/Real( nx_mesh );
  const Real meshsize_y   = (YB - YA)/Real( ny_mesh );
  const Real meshsize_z   = (ZB - ZA)/Real( nz_mesh );
  min_mesh_size           = std::min(meshsize_x, meshsize_y);
  min_mesh_size           = std::min(min_mesh_size, meshsize_z);
  max_mesh_size           = std::max(meshsize_x, meshsize_y);
  max_mesh_size           = std::max(min_mesh_size, meshsize_z);
  alpha = 0.6/max_mesh_size;
  //if (alpha > 0.5) alpha = 0.5;  // ksi = sqrt(PI)/3 = 0.590818
  
  
  const Real  drag_c      = 6.*PI*viscosity*Rb;    // Drag coefficient (N*s/um)
  const Real  q0          = Nks*bk;         // Maximum spring length (um)
  const Real chain_length = Ns*q0;          // contour length of the chain (um)
  const unsigned int Nb   = Ns + 1;         // # of beads
  const Real  Db          = kBT/drag_c;     // diffusivity of a bead (um^2/s)
  const Real  Dc          = Db/Real(Nb);    // diffusivity of the chain (um^2/s)
  const Real  Ss2         = Nks*bk*bk/6.;   // (um^2)
  //const Real  Ls0  = 0.9*q0/bead_r;         // initial spring lenght (dimensionless).
  
  // (5) characteristic variables
  const Real tc   = drag_c*Rb*Rb/kBT;       // diffusion time (s)
  const Real uc   = kBT/(drag_c*Rb);        // characteristic velocity (um/s)
  const Real fc   = kBT/Rb;                 // characteristic force (N)
  const Real muc  = 1./(6.*PI);             // non-dimensional viscosity
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * The larger the alpha, the sharper the g function, and the more singular the solution.
   * which requires finer mesh to resolve the force function g.
   * When alpha->inf, the modified Gaussian force g -> 3D delta function.
   
   * If we choose small alpha, the force function g becomes smooth, then can use coarsen mesh.
   * However, g will decay slowly, and a larger neigbhor list should be built in GGEM.
   * Therefore,  search_radius ~ 1/alpha
   
   * Usually take h <= 1/sqrt(2)/alpha: e.g. if alpha=0.1 => h<7.1
   * When alpha is too small, g function is very smooth and not vanishes at the domain bdry
   * Therefore, int[g(x)] over the domain is not equal to 1, the results are inaccurate!
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Output the selected parameters on the screen
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  std::cout << "\n\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The mesh and geometric info:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "nx_mesh = " << nx_mesh <<", Lx = " << XB-XA <<", hx = "<< meshsize_x <<"\n"
            << "ny_mesh = " << ny_mesh <<", Ly = " << YB-YA <<", hy = "<< meshsize_y <<"\n"
            << "nz_mesh = " << nz_mesh <<", Lz = " << ZB-ZA <<", hz = "<< meshsize_z <<"\n"
            << "fluid mesh size: hmin = " << min_mesh_size << ", hmax = " << max_mesh_size << "\n"
            << "the smoothing parameter in GGEM alpha = " << alpha << "\n";
  std::cout << " alpha*hmax = " << alpha*max_mesh_size << "\n";
  std::cout << std::endl;
  
  
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The Stokes solver info:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "Stokes solver type = " << stokes_solver_type << std::endl;
  if (stokes_solver_type=="field_split")
  {
    std::cout << "FieldSplit Schur Complement Reduction Solver\n";
    std::cout << "schur_pc_type = " << schur_pc_type << std::endl;
    if(schur_user_ksp)
    {
      std::cout<<"user defined KSP is used for Schur Complement!"<< std::endl;
      std::cout<<"KSP rel tolerance for Schur Complement solver is = " << schur_user_ksp_rtol <<"\n";
      std::cout<<"KSP abs tolerance for Schur Complement solver is = " << schur_user_ksp_atol <<"\n";
    }
  }
  std::cout << "\n\n";
  
  
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The physical parameters in the simulation:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "   viscosity                         mu  = " << viscosity <<" (cP = N*s/um^2)\n";
  std::cout << "                                     kBT = " << kBT << " (N*um = N*um)\n";
  std::cout << "   number of springs                 Ns  = " << Ns << "\n";
  std::cout << "   number of beads                   Nb  = " << Nb << "\n";
  std::cout << "   Kuhn length                       bk  = " << bk << " (um)\n";
  std::cout << "   # of Kuhn segment per spring      Nks = " << Nks << "\n";
  std::cout << "   Radius of the bead                a   = " << Rb << " (um)\n";
  std::cout << "   maximum spring length             q0  = " << q0 << " (um)\n";
  std::cout << "   chain length of polymer           Lc  = " << chain_length << " (um)\n";
  std::cout << "   bead diffusivity                  Db  = " << Db << " (um^2/s)\n";
  std::cout << "   chain diffusivity                 Dc  = " << Dc << " (um^2/s)\n";
  std::cout << "                                     Ss2 = " << Ss2 << " (um^2)\n";
  std::cout << "   HI Drag coefficient   zeta = 6*PI*mu*a = " << drag_c << " (N*s/um)\n";
  std::cout << "                      ksi = sqrt(PI)/(3a) = " << std::sqrt(PI)/(3.*Rb) <<"\n";
  std::cout << "   Excluded volume parameter          ev  = " << ev << " (um^3)\n";
  std::cout << "\n\n";
  
  
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The characteristic variables:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "   characteristic time          = " << tc << " (s)\n";
  std::cout << "   characteristic velocity      = " << uc << " (m/s)\n";
  std::cout << "   characteristic force         = " << fc << " (N)\n";
  std::cout << "\n\n";
  
  
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The non-dimensional variables:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "   non-dimensional bead radius      a0     = " << 1.0 << "\n";
  std::cout << "   non-dimensional Kuhn length    bk/a     = " << bk/Rb << "\n";
  std::cout << "   non-dimensional spring length  q0/a     = " << q0/Rb << "\n";
  std::cout << "   non-dimensional contour length Lc/a     = " << chain_length/Rb << "\n";
  std::cout << "   non-dimensional Ss/a = sqrt(Ss2/a^2)    = " << std::sqrt(Ss2/Rb/Rb) << "\n";
  std::cout << "   non-dimensional ksi = sqrt(PI)/(3a0)    = " << std::sqrt(PI)/(3.) << "\n";
  std::cout << "\n\n";
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * Create a mesh, distributed across the default MPI communicator.
   * We build a mesh with Quad9(8) elements for 2D and HEX27(20) element for 3D
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### Create finite element mesh:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  SerialMesh mesh(comm_in);   //Mesh mesh(comm_in);
  if(dim==2)
  MeshTools::Generation::build_square (mesh, nx_mesh, ny_mesh,
                                       XA, XB, YA, YB, QUAD8);        // QUAD8/9
  else if(dim==3)
  MeshTools::Generation::build_cube (mesh, nx_mesh, ny_mesh, nz_mesh,
                                     XA, XB, YA, YB, ZA, ZB, HEX20);  // HEX20/27
  else
  libmesh_example_requires(dim <= LIBMESH_DIM, "2D/3D support");
  // end if
  mesh.print_info();
  std::cout << "\n\n";
//  mesh.get_boundary_info().print_info();
//  return 0;
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Test the ParticleMesh/PointMesh class
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### Create periodic box, Polymer chain(s) and point-mesh:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  
  // add particle-mesh periodic boundary in the x-direction
  const Point bbox_pmin(XA, YA, ZA);
  const Point bbox_pmax(XB, YB, ZB);
  std::vector<bool> periodic_direction(dim,false);
//  periodic_direction[0] = true; // periodic boundary in x-direction
//  periodic_direction[1] = true; // periodic boundary in y-direction
//  periodic_direction[2] = true; // periodic boundary in z-direction
  PMPeriodicBoundary pm_periodicity(bbox_pmin, bbox_pmax, periodic_direction);
  
  
  
  // Construct PointMesh object from the polymer
  const Real search_radius_p = 4.0/alpha;
  const Real search_radius_e = 0.5*max_mesh_size + search_radius_p;
  PointMesh<3> point_mesh(mesh, search_radius_p, search_radius_e);
  point_mesh.add_periodic_boundary(pm_periodicity);
  
//  const std::string point_file = "polymer_data.in";
//  point_mesh.read_points_data(point_file);
  
  // - - - - - - - - - - - - Generate random beads - - - - - - - - - - - -
//  const std::size_t NRP = 1000000;  // 1M random points
//  point_mesh.generate_random_points(NRP,bbox_pmin, bbox_pmax);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  const std::string point_file = "random_points_file.txt";
  point_mesh.read_points_data(point_file);
  
//  point_mesh.reinit();
  
  
  
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The point-mesh info:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "Total number of particles: " << point_mesh.num_particles() << "\n";
  std::cout <<"search_radius_p = "<<search_radius_p <<", search_radius_e = "<<search_radius_e<<"\n";
  std::cout <<"Periodic BC in x-y-z directions: ";
  for (std::size_t i=0; i<dim; ++i)
  {
    if( periodic_direction[i] )
    {
      std::cout<<"  TRUE";
    }
    else
    {
      std::cout<<"  FALSE";
    }
  }
  std::cout << "\n\n\n";
  
//  if(comm_in.rank()==0)
//  {
//    point_mesh.print_point_info();
//    //point_mesh.print_elem_neighbor_list();
//  }
//  return 0;
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Create an equation systems and ParticleMeshLinearImplicitSystem "Stokes",
   and add variables: velocity (u, v, w) and pressure p. To satisfy LBB condition,
   (u, v, w):  second-order approximation
   p :         first-order basis
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  EquationSystems equation_systems (mesh);
  PMLinearImplicitSystem& system = equation_systems.add_system<PMLinearImplicitSystem> ("Stokes");
  unsigned int u_var = 0, v_var = 0, w_var = 0;
  u_var = system.add_variable ("u", SECOND);
  v_var = system.add_variable ("v", SECOND);
  if(dim==3)  w_var  = system.add_variable ("w", SECOND);
  const unsigned int p_var = system.add_variable ("p", FIRST);
  
  /* attach the particle-mesh system */
  system.attach_point_mesh(&point_mesh);
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Add periodic boundary conditions for the system.
   For the side number of a box, refer to libMesh::Hex27::build_side()
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  DofMap& dof_map = system.get_dof_map();
  
  /*** set PBC in x-direction ***/
  if (periodic_direction[0])
  {
    PeriodicBoundary pbcx(RealVectorValue(XB-XA, 0., 0.));
    pbcx.set_variable(u_var);
    pbcx.set_variable(v_var);
    if(dim==3) pbcx.set_variable(w_var);
    //pbcx.set_variable(p_var); //*** NOT include p!
    
    // is this boundary number still true for 3D?
    if(dim==2)
    {
      pbcx.myboundary = 3;
      pbcx.pairedboundary = 1;
    }
    else if(dim==3)
    {
      pbcx.myboundary = 4;
      pbcx.pairedboundary = 2;
    } // end if
    
    dof_map.add_periodic_boundary(pbcx);
    
    // check
    if (search_radius_p>=(XB-XA)/2. && comm_in.rank()==0)
    {
      printf("\n\n");
      printf("****************************** warning: ********************************\n");
      printf("**** The search radius is larger than the domain length in x direction! \n");
      printf("**** search radius = %f, domain size Lx = %f\n",search_radius_p,(XB-XA)/2.);
      printf("************************************************************************\n\n\n");
    }
  }
  
  /*** set PBC in y-direction ***/
  if (periodic_direction[1])
  {
    PeriodicBoundary pbcy(RealVectorValue(0., YB-YA, 0.));
    pbcy.set_variable(u_var);
    pbcy.set_variable(v_var);
    if(dim==3) pbcy.set_variable(w_var);
    //pbcy.set_variable(p_var); //*** NOT include p!
    
    if(dim==2)
    {
      pbcy.myboundary = 0;
      pbcy.pairedboundary = 2;
    }
    else if(dim==3)
    {
      pbcy.myboundary = 1;
      pbcy.pairedboundary = 3;
    } // end if
    
    dof_map.add_periodic_boundary(pbcy);
    
    // check
    if (search_radius_p>=(YB-YA)/2. && comm_in.rank()==0)
    {
      printf("\n\n");
      printf("****************************** warning: ********************************\n");
      printf("**** The search radius is larger than the domain length in y direction!\n");
      printf("**** search radius = %f, domain size Ly = %f\n",search_radius_p,(YB-YA)/2.);
      printf("************************************************************************\n\n\n");
    }
  }
  
  
  /*** set PBC in z-direction ***/
  if (periodic_direction[2])
  {
    PeriodicBoundary pbcz(RealVectorValue(0., 0., ZB-ZA));
    pbcz.set_variable(u_var);
    pbcz.set_variable(v_var);
    if(dim==3) pbcz.set_variable(w_var);
    //pbcz.set_variable(p_var); //*** NOT include p!
    
    if(dim==3)
    {
      pbcz.myboundary = 0;
      pbcz.pairedboundary = 5;
    } // end if
    
    dof_map.add_periodic_boundary(pbcz);
    
    // check
    if (search_radius_p>=(ZB-ZA)/2. && comm_in.rank()==0)
    {
      printf("\n\n");
      printf("****************************** warning: ********************************\n");
      printf("**** The search radius is larger than the domain length in z direction!\n");
      printf("**** search radius = %f, domain size Lz = %f\n",search_radius_p,(ZB-ZA)/2.);
      printf("************************************************************************\n\n\n");
    }
  }
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Initialize the Preconditioning matrix for saddle point problems if required.
   Initialize the equation system and zero the preconditioning matrix
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if( user_defined_pc ) system.add_matrix("Preconditioner");
  
  /* Initialize the data structures for the equation system. */
  equation_systems.init ();
  
  // zero the PC matrix, which MUST be done after es.init()
  if( user_defined_pc ) system.get_matrix("Preconditioner").zero();
  
  std::cout<<"###Equation systems are initialized:\n"<<std::endl;
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   set parameters of equations systems
   * NOTE: some of these parameters are not used if we use non-dimensional formulation
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  equation_systems.parameters.set<unsigned int>("linear solver maximum iterations") = max_linear_iterations;
  equation_systems.parameters.set<Real> ("linear solver rtol") = linear_solver_rtol;
  equation_systems.parameters.set<Real> ("linear solver atol") = linear_solver_atol;
  equation_systems.parameters.set<bool>    ("user_defined_pc") = user_defined_pc;
  equation_systems.parameters.set<bool>     ("schur_user_ksp") = schur_user_ksp;
  equation_systems.parameters.set<Real>("schur_user_ksp_rtol") = schur_user_ksp_rtol;
  equation_systems.parameters.set<Real>("schur_user_ksp_atol") = schur_user_ksp_atol;
  equation_systems.parameters.set<std::string>    ("schur_pc_type") = schur_pc_type;
  equation_systems.parameters.set<StokesSolverType> ("solver_type") = solver_type;
  
  equation_systems.parameters.set<Real>        ("XA_boundary") = XA;
  equation_systems.parameters.set<Real>        ("XB_boundary") = XB;
  equation_systems.parameters.set<Real>        ("YA_boundary") = YA;
  equation_systems.parameters.set<Real>        ("YB_boundary") = YB;
  equation_systems.parameters.set<Real>        ("ZA_boundary") = ZA;
  equation_systems.parameters.set<Real>        ("ZB_boundary") = ZB;
  equation_systems.parameters.set<Real>              ("alpha") = alpha;
  equation_systems.parameters.set<Real>   ("fluid mesh size")  = min_mesh_size;
  
  //equation_systems.parameters.set<Real>         ("viscosity")  = viscosity;
  //equation_systems.parameters.set<Real>               ("kBT")  = kBT;
  equation_systems.parameters.set<Real>       ("viscosity_0")  = muc;
  equation_systems.parameters.set<Real>               ("br0")  = 1.0;
  equation_systems.parameters.set<Real>               ("bk")   = bk;
  equation_systems.parameters.set<Real>               ("q0")   = q0;
  equation_systems.parameters.set<Real>       ("bead radius")  = Rb;
  equation_systems.parameters.set<Real>              ("drag")  = drag_c;
  equation_systems.parameters.set<Real>               ("Nks")  = Nks;
  equation_systems.parameters.set<Real>               ("Ss2")  = Ss2;
  equation_systems.parameters.set<Real>                ("ev")  = ev;
  equation_systems.parameters.set<Real>                ("tc")  = tc;
  equation_systems.parameters.set<std::string> ("particle_type")  = "point_particle";
  
  /* Print information about the mesh and system to the screen. */
  //mesh.print_info();
  equation_systems.print_info();
  std::cout <<"  System has: "<< mesh.n_elem()<<" elements,\n"
            <<"              "<< mesh.n_nodes()<<" nodes,\n"
            <<"              "<< equation_systems.n_dofs()<<" degrees of freedom.\n"
            <<"              "<< equation_systems.n_active_dofs()<<" active degrees of freedom.\n"
            <<"              "<< point_mesh.num_particles()<<" point particles.\n" << std::endl;
  
//  // write out the particle surface mesh
//  ExodusII_IO(mesh).write_equation_systems("output_test_pm_system.e",equation_systems);
//  return 0;
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Init the force field and attach it to the PMLinearImplicitSystem pm_system
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ForceField force_field(system);
  system.attach_force_field(&force_field);
  
  
  
  /* ------------------------------------------------------------------------------------*/
  /* ---------------------------------- TEST PROGRAM ----------------------------------- */
  /* ------------------------------------------------------------------------------------*/
  Real t0 = MPI_Wtime();
  std::string msg = "--->TEST reinit particle mesh:";
  PMToolBox::output_message(msg,comm_in);
  system.reinit_system();
//  if(comm_in.rank()==0)
//  {
//    point_mesh.print_point_info();
//    //point_mesh.print_elem_neighbor_list();
//  }
  Real t1 = MPI_Wtime();
  
  
  system.test_velocity_profile();
  //  system.test_l2_norm();
  Real t2 = MPI_Wtime();
  
  
  msg = "--->TEST Stokes solver with point forces:";
  PMToolBox::output_message(msg,comm_in);
  bool re_init_stokes = true;
//  system.solve_stokes("undisturbed",re_init_stokes);
//  ExodusII_IO(mesh).write_equation_systems("output_speedup_test_pm_system0.e",equation_systems);
  
  re_init_stokes = false;
  system.solve_stokes("disturbed",re_init_stokes);
  Real t3 = MPI_Wtime();
  
  system.add_local_solution();
  ExodusII_IO(mesh).write_equation_systems("output_speedup_test01_pm_system1.e",equation_systems);
  
  const bool write_velocity = false;
  std::vector<Real> pv_temp;
  system.write_point_csv("output_test_point.csv", pv_temp, write_velocity);
  Real t4 = MPI_Wtime();
  
  std::cout << "---------------------------------------------------------------\n";
  std::cout << "reinit_system uses         " << t1-t0 << " s\n";
  std::cout << "test_velocity_provile uses " << t2-t1 << " s\n";
  std::cout << "The 2nd solve_stokes use   " << t3-t2 << " s\n";
  std::cout << "add local solution & output: " << t4-t3 << " s\n";
  std::cout << "---------------------------------------------------------------\n";
  return 0;
  
  
} // end of function



#endif /* test_scalability_01_h */
