//
//  assemble_navier_stokes.h
//  
//
//  Created by Xujun Zhao on 12/9/15.
//
//

#ifndef assemble_navier_stokes_h
#define assemble_navier_stokes_h


// C++ Includes
#include <stdio.h>
#include <iostream>
#include <cstring>
#include <utility>
#include <vector>
#include <map>

// libmesh includes
#include "libmesh/libmesh.h"
#include "libmesh/equation_systems.h"
#include "libmesh/point.h"


// Bring in everything from the libMesh namespace
using namespace libMesh;


/*
 * This class provides the basic components
 * for assembling the matrix and vector when solving
 * Navier-Stokes equations.
 * 
 * For details of the numerical discretization, refer to
 * The finite element method in heat transfer and fluid dynamics (3rd ed)
 * J.N. Reddy and D.K. Gartling. 2010, CRC Press
 */

class AssembleNS : public ReferenceCountedObject<AssembleNS>
{
public:
  // Constructor
  AssembleNS(EquationSystems& es);
  
  
  // Destructor
  ~AssembleNS();


  // Assemble the Global Matrix K
  void assemble_global_K(const std::string& system_name,
                         const std::string& option);
  
  
  // Assemble the Global force vector F
  void assemble_global_F(const std::string& system_name,
                         const std::string& option);
  
  
  // Assemble the element matrix K_IJ
  void assemble_element_KIJ(const Elem*     elem,
                            const std::vector<Real>& JxW,
                            const std::vector<std::vector<RealGradient> >& dphi,
                            const Real&     mu,
                            const unsigned int n_u_dofs,
                            const unsigned int I,
                            const unsigned int J,
                            DenseMatrix<Number>& Kij);

  
  // Assemble the element matrix Q_I: Q matrix (Kup, Kvp, Kwp),
  void assemble_element_QI(const Elem*     elem,
                           const std::vector<Real>& JxW,
                           const std::vector<std::vector<RealGradient> >& dphi,
                           const std::vector<std::vector<Real> >& psi,
                           const unsigned int n_v_dofs,
                           const unsigned int n_p_dofs,
                           const unsigned int I,
                           DenseMatrix<Number>& Qi);
  
  
  // Assemble the element mass matrix M.
  void assemble_element_MIJ(const Elem*     elem,
                            const std::vector<Real>& JxW,
                            const std::vector<std::vector<Real> >& phi,
                            const Real&     rho,
                            const unsigned int n_v_dofs,
                            DenseMatrix<Number>& Mij);
  
  
  // assemble function for slit channel
  void compute_element_rhs(const Elem*     elem,
                           const unsigned int n_u_dofs,
                           FEBase& fe_v,
                           const std::vector<std::size_t> n_list,
                           const bool& pf_flag,
                           const std::string& option,
                           const Real& alpha,
                           const Real& rho,
                           DenseVector<Number>& Fe);
  
  // Apply BCs by penalty method.
  void apply_bc_by_penalty(const Elem* elem,
                           const std::string& matrix_or_vector,
                           DenseMatrix<Number>& Ke,
                           DenseVector<Number>& Fe,
                           const std::string& option);
    
  
  // Penalize element matrix or vector with a large number.
  void penalize_elem_matrix_vector(DenseMatrix<Number>& Ke,
                                   DenseVector<Number>& Fe,
                                   const std::string & matrix_or_vector,
                                   const unsigned int& var_number,     // variable number
                                   const unsigned int& local_node_id,  // local node id to be penalized
                                   const unsigned int& n_nodes_elem,   // vel-node number of elem!
                                   const Real& penalty,
                                   const Real& value);
  
  
  // define the pressure jump at the inlet and outlet of the channel
  Real boundary_pressure_jump(const Point& pt,
                              const std::string& which_side) const;
  
  
  // define the pressure jump at the inlet and outlet of the channel
  Real boundary_traction(const Point& pt,
                         const std::string& which_side) const;
  
  
private:
  // Equation systems
  EquationSystems& _eqn_sys;

  
  // system dimension (mesh dimension)
  unsigned int _dim;
  
};

#endif /* assemble_navier_stokes_h */
