//
//  test_sedimentation_01.h
//  
//
//  Created by Xujun Zhao on 2/27/16.
//
//

#ifndef test_sedimentation_01_h
#define test_sedimentation_01_h


#include <fstream>

#include "libmesh/getpot.h"
#include "libmesh/mesh.h"
#include "libmesh/serial_mesh.h"
#include "libmesh/mesh_generation.h"
#include "libmesh/mesh_modification.h"
#include "libmesh/mesh_refinement.h"

#include "libmesh/gmv_io.h"
#include "libmesh/exodusII_io.h"

#include "libmesh/dof_map.h"
#include "libmesh/linear_solver.h"
#include "libmesh/equation_systems.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/periodic_boundary.h"
#include "libmesh/sparse_matrix.h"
#include "libmesh/numeric_vector.h"


// include user defined classes or functions
#include "point_particle.h"
#include "rigid_particle.h"
#include "particle_mesh.h"
#include "point_mesh.h"
#include "force_field.h"
#include "pm_linear_implicit_system.h"
#include "mesh_spring_network.h"
#include "brownian_system.h"
#include "pm_periodic_boundary.h"
#include "chebyshev.h"
#include "pm_toolbox.h"



int test_sedimentation_01(const Parallel::Communicator &comm_in)
{
  
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### running test_sedimentation_01\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Define constants: We use um as the length unit
   kB = 1.380662E-23(J/K) = 1.380662E-23(N*m/K) = 1.380662E-17 (N*um/K)
   T  = 297 K
   ===> kB*T = 4.1E-15 (N*um)
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  const Real PI     = libMesh::pi;
  const Real kBT    = 4.1E-15;     //
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Parse the input file and read in parameters from the input file
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  GetPot input_file("rigid_particle_control.in");
  const bool  restart       = input_file("restart", false);
  std::size_t restart_step  = input_file("restart_step", 0);
  std::size_t random_seed   = input_file("random_seed",111);
  Real        restart_time  = input_file("restart_time", 0.0);
  if(restart) // update the seed for restart mode
  {
    random_seed++;
  }
  else        // set the restart_step as zero
  {
    restart_step = 0;
    restart_time = 0.0;
  }
  
  
  // (1) Stokes solver parameters
  const int max_linear_iterations = input_file("max_linear_iterations", 100);
  const Real linear_solver_rtol   = input_file("linear_solver_rtol", 1E-6);
  const Real linear_solver_atol   = input_file("linear_solver_atol", 1E-6);
  bool  user_defined_pc           = input_file("user_defined_pc", true);
  const bool schur_user_ksp       = input_file("schur_user_ksp", false);
  const Real schur_user_ksp_rtol  = input_file("schur_user_ksp_rtol", 1E-6);
  const Real schur_user_ksp_atol  = input_file("schur_user_ksp_atol", 1E-6);
  const std::string schur_pc_type = input_file("schur_pc_type", "SMp");
  const std::string stokes_solver_type = input_file("stokes_solver", "superLU_dist");
  StokesSolverType solver_type; // enum is claimed in StokesSolver class
  if(stokes_solver_type=="superLU_dist") {
    solver_type = superLU_dist;
    user_defined_pc = false;
  }
  else if(stokes_solver_type=="field_split") {
    solver_type = field_split;
    user_defined_pc = true;
  }
  else {
    solver_type = user_define;
  }

  
  
  // (2) Mesh & Geometry parameters
  const unsigned int dim          = input_file("dimension", 3);
  const Real XA                   = input_file("XA", -1.);
  const Real XB                   = input_file("XB", +1.);
  const Real YA                   = input_file("YA", -1.);
  const Real YB                   = input_file("YB", +1.);
  Real ZA                         = input_file("ZA", -1.);
  Real ZB                         = input_file("ZB", +1.);
  if (dim==2){  ZA = 0.;  ZB = 0.;  }
  const unsigned int nx_mesh      = input_file("nx_mesh", 20);
  const unsigned int ny_mesh      = input_file("ny_mesh", 10);
  const unsigned int nz_mesh      = input_file("nz_mesh", 10);
  const Real alpha                = input_file("alpha", 0.1);
  
  
  // (3) Physical parameters
  const Real viscosity            = input_file("viscosity", 1.0); // viscosity N*s/um^2
  const Real ev                   = input_file("ev", 1E-3); // excluded volume parameter
  
  
  // (4) Compute other parameters using the input
  Real min_mesh_size, max_mesh_size;
  const Real meshsize_x   = (XB - XA)/Real( nx_mesh );
  const Real meshsize_y   = (YB - YA)/Real( ny_mesh );
  const Real meshsize_z   = (ZB - ZA)/Real( nz_mesh );
  min_mesh_size           = std::min(meshsize_x, meshsize_y);
  min_mesh_size           = std::min(min_mesh_size, meshsize_z);
  max_mesh_size           = std::max(meshsize_x, meshsize_y);
  max_mesh_size           = std::max(min_mesh_size, meshsize_z);
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Output the selected parameters on the screen
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  std::cout << "\n\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The mesh and geometric info:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "nx_mesh = " << nx_mesh <<", Lx = " << XB-XA <<", hx = "<< meshsize_x <<"\n"
            << "ny_mesh = " << ny_mesh <<", Ly = " << YB-YA <<", hy = "<< meshsize_y <<"\n"
            << "nz_mesh = " << nz_mesh <<", Lz = " << ZB-ZA <<", hz = "<< meshsize_z <<"\n"
            << "minimum mesh size of fluid: hmin = " << min_mesh_size << "\n"
            << "the smoothing parameter in GGEM alpha = " << alpha << "\n";
            std::cout << std::endl;
  
  
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The Stokes solver info:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "Stokes solver type = " << stokes_solver_type << std::endl;
  if (stokes_solver_type=="field_split")
  {
    std::cout << "FieldSplit Schur Complement Reduction Solver\n";
    std::cout << "schur_pc_type = " << schur_pc_type << std::endl;
    if(schur_user_ksp)
    {
      std::cout<<"user defined KSP is used for Schur Complement!"<< std::endl;
      std::cout<<"KSP rel tolerance for Schur Complement solver is = " << schur_user_ksp_rtol <<"\n";
      std::cout<<"KSP abs tolerance for Schur Complement solver is = " << schur_user_ksp_atol <<"\n";
    }
  }
  std::cout << "\n\n";

  
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The physical parameters in the simulation:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "   viscosity                          mu  = " << viscosity <<" (N*s/um^2)\n";
  std::cout << "   Excluded volume parameter          ev  = " << ev << " (um^3)\n";
  std::cout << "\n\n";
  

  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * Create a mesh, distributed across the default MPI communicator.
   * We build a mesh with Quad9(8) elements for 2D and HEX27(20) element for 3D
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### Create finite element mesh of the fluid:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  SerialMesh mesh(comm_in);   //Mesh mesh(comm_in);
  if(dim==2){
    MeshTools::Generation::build_square (mesh, nx_mesh, ny_mesh,
                                         XA, XB, YA, YB, QUAD8);        // QUAD8/9
  }
  else if(dim==3){
    MeshTools::Generation::build_cube (mesh, nx_mesh, ny_mesh, nz_mesh,
                                       XA, XB, YA, YB, ZA, ZB, HEX20);  // HEX20/27
  }
  else {
    libmesh_example_requires(dim <= LIBMESH_DIM, "2D/3D support");
  }
  // end if
  mesh.print_info();
  std::cout << "\n\n";
  
  
  
  // Names of surface and volume mesh files
  const std::string vmesh = "volume_mesh_coarse.e"; // coarse/medium/fine
  const std::string smesh = "surface_mesh_sphere.e";
//  const std::string vmesh = "cylinder_3D_vol_mesh_level_7.e"; // coarse/medium/fine (cylinder)
//  const std::string smesh = "surface_mesh_cylinder.e";
  const std::string mesh_type = "surface_mesh";
//  const std::string mesh_type = "volume_mesh";
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Test the ParticleMesh/PointMesh class
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### Create particle-mesh and point-mesh system:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  
  // add particle-mesh periodic boundary in the x-direction
  const Point bbox_pmin(XA, YA, ZA);
  const Point bbox_pmax(XB, YB, ZB);
  std::vector<bool> periodic_direction(dim,false);
  periodic_direction[0] = true; // periodic boundary in x-direction
//  periodic_direction[1] = true; // periodic boundary in y-direction
//  periodic_direction[2] = true; // periodic boundary in z-direction
  PMPeriodicBoundary pm_periodicity(bbox_pmin, bbox_pmax, periodic_direction);
  
  // Read the particle data and surface mesh
  const std::string pfilename = "rigid_particle_data.in";
  const Real search_radius_p = 4.0/alpha;
  const Real search_radius_e = 0.5*max_mesh_size + search_radius_p;
  ParticleMesh<3> particle_mesh(mesh,search_radius_p,search_radius_e);
  particle_mesh.add_periodic_boundary(pm_periodicity);
  particle_mesh.read_particles_data(pfilename,vmesh,smesh,mesh_type);
  particle_mesh.reinit();
  
  
  // Construct the PointMesh class from the ParticleMesh
  PointMesh<3> point_mesh(particle_mesh,search_radius_p,search_radius_e);
  point_mesh.reinit();
  
  
  /// ------------------------- TEST ParticleMesh function ---------------------------
  for (std::size_t i=0; i<particle_mesh.num_particles(); ++i)
  {
    Real vol  = particle_mesh.particles()[i]->compute_volume();
    Real area = particle_mesh.particles()[i]->compute_area();
    Point pc_i = particle_mesh.particles()[i]->compute_centroid(mesh_type);
    const std::vector<Real> hs = particle_mesh.particles()[i]->mesh_size();
    //particle_mesh.particles()[i]->compute_surface_normal(mesh_type);
    
    if(comm_in.rank()==0)
    {
      printf("------> Volume of the %lu-th particle is %f, and area = %f\n",i,vol,area);
      const Real Ri = particle_mesh.particles()[i]->radius();
      const Real vol_th = 4./3.*PI*Ri*Ri*Ri, area_th = 4.*PI*Ri*Ri;
      printf("        The theoretical value of V is %f, rel error = %f\n",
             vol_th,std::abs(vol-vol_th)/vol_th);
      printf("        The theoretical value of A is %f, rel error = %f\n",
             area_th,std::abs(area-area_th)/area_th);
      printf("        The centroid = (%f,%f,%f)\n\n", pc_i(0),pc_i(1),pc_i(2));
      printf("        hmin = %f, hmax = %f\n\n", hs[0],hs[1] );
    }
  }
  /// --------------------------------------------------------------------------------
  
  
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The particle-mesh and point-mesh info:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "Total number of particles: " << particle_mesh.num_particles() << "\n";
  std::cout << "Total number of points: " << particle_mesh.num_mesh_points() << "\n";
  for(std::size_t i=0; i<particle_mesh.num_particles(); ++i)
  {
    const std::size_t n_surf_elem = particle_mesh.particles()[i]->num_mesh_elem();
    const std::size_t n_surf_node = particle_mesh.particles()[i]->num_mesh_nodes();
    std::cout << "  Particle "<<i<<": center " << particle_mesh.particles()[i]->center() <<"\n";
    std::cout << "         " << n_surf_elem << " elements for the particle's mesh" << "\n";
    std::cout << "         " << n_surf_node << " nodes  for the particle's mesh" << "\n";
  }
  
  std::cout <<"search_radius_p = "<<search_radius_p <<", search_radius_e = "<<search_radius_e<<"\n";
  std::cout <<"Periodic BC in x-y-z directions: ";
  for (std::size_t i=0; i<dim; ++i)
  {
    if( periodic_direction[i] ){
      std::cout<<"  TRUE";
    }
    else{
      std::cout<<"  FALSE";
    }
  }
  std::cout << "\n\n\n";
  //if(comm_in.rank()==0) point_mesh.print_point_info();
  //return 0;
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Create an equation systems and ElasticitySystem on top of particle->mesh()
   and add variables: displacement (U, V, W) with all first order
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  EquationSystems elasticity_es (particle_mesh.particles()[0]->mesh());
  ElasticitySystem& elasticity_system = elasticity_es.add_system<ElasticitySystem> ("Elasticity");
  unsigned int U_var = 0, V_var = 0, W_var = 0;
  U_var = elasticity_system.add_variable ("U", FIRST);
  V_var = elasticity_system.add_variable ("V", FIRST);
  if(dim==3)  W_var  = elasticity_system.add_variable ("W", FIRST);
  const std::vector<Real> hsize_solid = elasticity_system.mesh_size();// mesh size of solid
  elasticity_es.init();
  
  elasticity_es.print_info();
  MeshBase& elasticity_mesh = elasticity_system.get_mesh();
  std::cout <<"  Elasticity System has: "<< elasticity_mesh.n_elem()<<" elements,\n"
            <<"              "<< elasticity_mesh.n_nodes()<<" nodes,\n"
            <<"              "<< "hmin = " << hsize_solid[0] << ", hmax = "<< hsize_solid[1]<<"\n"
            <<"              "<< elasticity_es.n_active_dofs()<<" active degrees of freedom.\n"
            <<"              "<< particle_mesh.num_particles()<<" particles.\n" << std::endl;
  

  
  
  /* - - - - - - - - - - - - TEST - - - - - - - - - - - - -
   * Compute force density at each node due to gravity
   * - - - - - - - - - - - -- - - - - - - - - - - -- - - - */
  std::vector<Real> force_density(3);
  force_density[0]    = 0.1;  // f = rho*g and F = f*V
  if(mesh_type == "surface_mesh")
  {
    const Real particle_radius = particle_mesh.particles()[0]->radius();
    const Real VS_Ratio = particle_radius/3.0;  // volume to area ratio = R/3
    for(std::size_t k=0; k<3; ++k){
      force_density[k] *= VS_Ratio;
    }
  }
  elasticity_system.build_nodal_force_gravity(force_density);
//  ExodusII_IO(elasticity_es.get_mesh()).write_equation_systems("elasticity_system_1.e",
//                                                               elasticity_es);
  //return 0;
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Create an equation systems and ParticleMeshLinearImplicitSystem,
   and add variables: velocity (u, v, w) and pressure p. To satisfy LBB condition,
   (u, v, w):  second-order approximation
   p :         first-order basis
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  EquationSystems stokes_es (mesh);
  PMLinearImplicitSystem& pm_system = stokes_es.add_system<PMLinearImplicitSystem> ("Stokes");
  unsigned int u_var = 0, v_var = 0, w_var = 0;
  u_var = pm_system.add_variable ("u", SECOND);
  v_var = pm_system.add_variable ("v", SECOND);
  if(dim==3)  w_var  = pm_system.add_variable ("w", SECOND);
  const unsigned int p_var = pm_system.add_variable ("p", FIRST);
  
  /* attach the particle-mesh system */
  pm_system.attach_particle_mesh(&particle_mesh);
  pm_system.attach_point_mesh(&point_mesh);
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Add periodic boundary conditions for the system.
   For the side number of a box, refer to libMesh::Hex27::build_side()
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  DofMap& dof_map = pm_system.get_dof_map();
  
  /*** set PBC in x-direction ***/
  if (periodic_direction[0])
  {
    PeriodicBoundary pbcx(RealVectorValue(XB-XA, 0., 0.));
    pbcx.set_variable(u_var);
    pbcx.set_variable(v_var);
    if(dim==3) pbcx.set_variable(w_var);
    //    pbcx.set_variable(p_var); //*** NOT include p!
    
    // boundary number
    if(dim==2)
    {
      pbcx.myboundary = 3;
      pbcx.pairedboundary = 1;
    }
    else if(dim==3)
    {
      pbcx.myboundary = 4;
      pbcx.pairedboundary = 2;
    } // end if
    
    dof_map.add_periodic_boundary(pbcx);
    
    // check
    if (search_radius_p>=(XB-XA)/2. && comm_in.rank()==0)
    {
      printf("\n\n");
      printf("****************************** warning: ********************************\n");
      printf("**** The search radius is larger than the domain length in x direction! \n");
      printf("**** search radius = %f, domain size Lx = %f\n",search_radius_p,(XB-XA)/2.);
      printf("************************************************************************\n\n\n");
    }
  }
  
  /*** set PBC in y-direction ***/
  if (periodic_direction[1])
  {
    PeriodicBoundary pbcy(RealVectorValue(0., YB-YA, 0.));
    pbcy.set_variable(u_var);
    pbcy.set_variable(v_var);
    if(dim==3) pbcy.set_variable(w_var);
    //    pbcy.set_variable(p_var); //*** NOT include p!
    
    if(dim==2)
    {
      pbcy.myboundary = 0;
      pbcy.pairedboundary = 2;
    }
    else if(dim==3)
    {
      pbcy.myboundary = 1;
      pbcy.pairedboundary = 3;
    } // end if
    
    dof_map.add_periodic_boundary(pbcy);
    
    // check
    if (search_radius_p>=(YB-YA)/2. && comm_in.rank()==0)
    {
      printf("\n\n");
      printf("****************************** warning: ********************************\n");
      printf("**** The search radius is larger than the domain length in y direction!\n");
      printf("**** search radius = %f, domain size Ly = %f\n",search_radius_p,(YB-YA)/2.);
      printf("************************************************************************\n\n\n");
    }
  }
  
  /*** set PBC in y-direction ***/
  if (periodic_direction[2])
  {
    PeriodicBoundary pbcz(RealVectorValue(0., 0., ZB-ZA));
    pbcz.set_variable(u_var);
    pbcz.set_variable(v_var);
    if(dim==3) pbcz.set_variable(w_var);
    //pbcz.set_variable(p_var); //*** NOT include p!
    
    if(dim==3)
    {
      pbcz.myboundary = 0;
      pbcz.pairedboundary = 5;
    } // end if
    
    dof_map.add_periodic_boundary(pbcz);
    
    // check
    if (search_radius_p>=(ZB-ZA)/2. && comm_in.rank()==0)
    {
      printf("\n\n");
      printf("****************************** warning: ********************************\n");
      printf("**** The search radius is larger than the domain length in z direction!\n");
      printf("**** search radius = %f, domain size Lz = %f\n",search_radius_p,(ZB-ZA)/2.);
      printf("************************************************************************\n\n\n");
    }
  }
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Initialize the Preconditioning matrix for saddle point problems if required.
   Initialize the equation system and zero the preconditioning matrix
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if( user_defined_pc ) pm_system.add_matrix("Preconditioner");
  
  
  /* Initialize the data structures for the equation system. */
  stokes_es.init ();
  
  // zero the PC matrix, which MUST be done after es.init()
  if( user_defined_pc ) pm_system.get_matrix("Preconditioner").zero();
  
  std::cout<<"###Equation systems are initialized:\n"<<std::endl;
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   set parameters of equations systems
   * NOTE: some of these parameters are not used if we use non-dimensional formulation
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  stokes_es.parameters.set<unsigned int>("linear solver maximum iterations") = max_linear_iterations;
  stokes_es.parameters.set<Real>   ("linear solver rtol") = linear_solver_rtol;
  stokes_es.parameters.set<Real>   ("linear solver atol") = linear_solver_atol;
  stokes_es.parameters.set<bool>      ("user_defined_pc") = user_defined_pc;
  //stokes_es.parameters.set<bool>     ("schur_complement") = schur_complement;
  stokes_es.parameters.set<bool>       ("schur_user_ksp") = schur_user_ksp;
  stokes_es.parameters.set<Real>  ("schur_user_ksp_rtol") = schur_user_ksp_rtol;
  stokes_es.parameters.set<Real>  ("schur_user_ksp_atol") = schur_user_ksp_atol;
  stokes_es.parameters.set<std::string> ("schur_pc_type") = schur_pc_type;
  stokes_es.parameters.set<StokesSolverType> ("solver_type") = solver_type;
  
  stokes_es.parameters.set<Real>        ("XA_boundary") = XA;
  stokes_es.parameters.set<Real>        ("XB_boundary") = XB;
  stokes_es.parameters.set<Real>        ("YA_boundary") = YA;
  stokes_es.parameters.set<Real>        ("YB_boundary") = YB;
  stokes_es.parameters.set<Real>        ("ZA_boundary") = ZA;
  stokes_es.parameters.set<Real>        ("ZB_boundary") = ZB;
  stokes_es.parameters.set<Real>              ("alpha") = alpha;
  stokes_es.parameters.set<Real>    ("fluid mesh size") = min_mesh_size;
  stokes_es.parameters.set<Real>    ("solid mesh size") = hsize_solid[0];
  
  stokes_es.parameters.set<Real>         ("viscosity")  = viscosity;
  stokes_es.parameters.set<Real>       ("viscosity_0")  = 1./(6.*PI);
  stokes_es.parameters.set<Real>               ("br0")  = 1.0;
  stokes_es.parameters.set<Real>               ("kBT")  = kBT;
  stokes_es.parameters.set<Real>                ("ev")  = ev;
  stokes_es.parameters.set<std::string> ("particle_type")  = "rigid_particle";
  stokes_es.parameters.set<std::string> ("particle_mesh_type")  = mesh_type;
  
  /* Print information about the mesh and system to the screen. */
  //mesh.print_info();
  std::cout <<"  Stokes System has: "<< mesh.n_elem()<<" elements,\n"
            <<"              "<< mesh.n_nodes()<<" nodes,\n"
            <<"              "<< stokes_es.n_active_dofs()<<" active degrees of freedom.\n"
            <<"              "<< particle_mesh.num_particles()<<" particles.\n" << std::endl;
  stokes_es.print_info();
  
  //return 0;
  /* ------------------------------------------------------------------------------------*/
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   build MeshSpringNetwork on the surface mesh, which will be used to apply
   the rigid-body force.
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  MeshBase& p_mesh = particle_mesh.particles()[0]->mesh();
  const Point center0 = particle_mesh.particles()[0]->center();
  MeshSpringNetwork mesh_spring_network(p_mesh,pm_periodicity);
  mesh_spring_network.build_spring_network(center0);
  //if(comm_in.rank()==0) mesh_spring_network.print_info();
  elasticity_system.attach_mesh_spring_network(&mesh_spring_network);
  
  
  // attach the mesh spring network
  for(std::size_t i=0; i<particle_mesh.num_particles(); ++i)
  {
    particle_mesh.particles()[i]->attach_mesh_spring_network(&mesh_spring_network);
  }
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Init the force field and attach it to the PMLinearImplicitSystem pm_system
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ForceField force_field(pm_system, elasticity_system);
  pm_system.attach_force_field(&force_field);
  //return 0;
  
  
  
  /* ------------------------------------------------------------------------------------*/
  /* ---------------------------------- TEST PROGRAM ----------------------------------- */
  /* ------------------------------------------------------------------------------------*/
//  std::string msg = "--->TEST reinit particle mesh:";
//  PMToolBox::output_message(msg,comm_in);
//  pm_system.reinit_system();
////  const std::string point_output_filename = "points.csv." + std::to_string(0);
////  pm_system.write_point_csv(point_output_filename);
////  if(comm_in.rank()==0)
////  {
////    point_mesh.print_point_info();
////    //point_mesh.print_elem_neighbor_list();
////  }
//  
//  msg = "--->TEST Stokes solver with point forces:";
//  PMToolBox::output_message(msg,comm_in);
//  bool re_init_stokes = true;
//  pm_system.solve_stokes("undisturbed",re_init_stokes);
//  ExodusII_IO(mesh).write_equation_systems("pm_system0.e",stokes_es);
//  
//  re_init_stokes = false;
//  pm_system.solve_stokes("disturbed",re_init_stokes);
//  pm_system.add_local_solution();
//  ExodusII_IO(mesh).write_equation_systems("pm_system1.e",stokes_es);
//  particle_mesh.write_particle_mesh("particles_mesh_test0.e");
//  return 0;
  
  
  
  
  
  
  /* ------------------------------------------------------------------------------------
   * ------------------------------ TEST Moving particles -------------------------------
   (1) change mesh file name;
   (2) change BCs
   (3) change bead-wall distance function
   (4) change the check_wall function for different geometries
   (5) change external force acting on the beads.
   (6) scp -r zhaox@blues.lcrc.anl.gov:~/project/colloid_hd/ex04_sedimentation_a ./
   * ------------------------------------------------------------------------------------*/
  std::string msg6 = "--->TEST Moving point particles:";
  PMToolBox::output_message(msg6,comm_in);
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Collect system parameters for the simulation
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  const unsigned int NP     = point_mesh.num_particles();
  const unsigned int n_vec  = dim*NP;
  const unsigned int n_cheb = 50;
  const Real       tol_cheb = 0.1;
  const Real     eig_factor = 1.05;
  const Real      tol_eigen = 0.01;
  bool  cheb_converge, compute_eigen = true;
  Real eig_min = 0., eig_max = 0., real_time = 0.0;
  
  
  //const Real          Ss2_a2 = Ss2/Rb/Rb; // dt =  c*Ss2/a2
  const Real            dt0  = 0.1;
  const unsigned int  nstep  = 10;
  const unsigned int write_interval = 1;
  const bool  with_brownian = false;
  //  pm_system.test_move_particles(dt0,nstep,write_interval,with_brownian);  return 0;
  
  std::ostringstream  oss;
  const bool  write_es   =  true;
  const bool  print_info =  false;
  const Real  hminf = stokes_es.parameters.get<Real>("fluid mesh size");
  const Real  hmins = stokes_es.parameters.get<Real>("solid mesh size");
  const std::string particle_type = stokes_es.parameters.get<std::string>("particle_type");
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Compute undisturbed velocity field without particles.
   NOTE: We MUST re-init particle-mesh before solving Stokes
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  oss << "Computing the undisturbed velocity field ......";
  PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
  pm_system.reinit_system();
  bool reinit_stokes = true;
  pm_system.solve_stokes("undisturbed",reinit_stokes);
  UniquePtr<NumericVector<Real>> v0_ptr = pm_system.solution->clone(); // backup v0
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Print out the particle-mesh information if needed
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  comm_in.barrier();
  if ( print_info && (comm_in.rank() == 0) )
  {
    oss << "Print out point-mesh information at step 0.";
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    if(particle_type=="rigid_particle"){
      particle_mesh.print_particle_info();
      particle_mesh.print_elem_neighbor_list();
    }
    point_mesh.print_point_info();
    point_mesh.print_elem_neighbor_list();
  }
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   write out the equation systems if write_es = true at Step 0(undisturbed)
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  oss << "Write out particle-mesh system into the local file at step 0.";
  PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
  const std::string out_filename   = "pm_system";
  ExodusII_IO*  exodus_ptr;
  if(write_es)
  {
    //system.add_local_solution(); // Don't add local solution for undisturbed system!
#ifdef LIBMESH_HAVE_EXODUS_API
    exodus_ptr = new ExodusII_IO(mesh);
    exodus_ptr->write_equation_systems(out_filename+".e", stokes_es);
#endif
  }   // end if( write_es )
  
  
  /* Write particles' surface mesh at the step 0 */
  const std::string smesh_file_name = "particle_surface_mesh.e";
  if(particle_type=="rigid_particle"){
    particle_mesh.write_particle_mesh(smesh_file_name);
  }
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Create vectors and Shell Mat for use:
   U0:          particle velocity vector;
   R0/R_mid:    particle position vector;
   dw/dw_mid:   random vector;
   RIN/ROUT:    the initial and intermediate particle postion vector for msd output
   RIN will not change, and ROUT excludes pbc
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  Vec             U0, R0, R_mid, RIN,ROUT, dw, dw_mid;
  Mat             M;
  PetscRandom     rand_ctx;
  PetscScalar     coef = 0.0;
  BrownianSystem brownian_sys (stokes_es);
  brownian_sys.init_petsc_random(&rand_ctx);
  brownian_sys._create_shell_mat(n_vec, &M);
  brownian_sys._create_petsc_vec(n_vec,&R0);
  VecDuplicate(R0,&U0);
  VecDuplicate(R0,&R_mid);
  VecDuplicate(R0,&dw_mid);
  brownian_sys.extract_particle_vector(&ROUT,"coordinate","extract");
  VecDuplicate(ROUT,&RIN);
  VecCopy(ROUT,RIN);  // RIN = ROUT = the initial position vector
  
  
  /* Output mean square displacement and radius of gyration at step 0 */
  Real  Rg      = brownian_sys.radius_of_gyration(ROUT,dim);
  Point chain_S = brownian_sys.chain_stretch(ROUT,dim); //(stretch)
  std::ofstream out_msd;
  int o_width = 10, o_precision = 9;
  if(comm_in.rank()==0)
  {
    out_msd.open("msd.txt",std::ios_base::out);
    out_msd.setf(std::ios::right);    out_msd.setf(std::ios::fixed);
    out_msd.precision(o_precision);   out_msd.width(o_width);
    out_msd << 0 << " " << 0.0 << " " << 0.0 << " " << Rg << " "; // step | time | msd | Rg
    out_msd << chain_S(0) << " " << chain_S(1) << " " << chain_S(2) << "\n";// Sx | Sy | Sz
    out_msd.close();
  }
  
  
  /* - - - - - - - - - - - - - - - Write CSV file  - - - - - - - - - - - - - - - */
//  oss << "points.csv." << 0;
//  std::string csv_file_name = oss.str();
//  pm_system.write_point_csv(csv_file_name,&U0,false); // write_velocity = false
//  oss.str(""); oss.clear();
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Advancing in time. Fixman Mid-Point algorithm
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  unsigned int o_step = 0;  // output step
  for(unsigned int i=0; i<nstep; ++i)
  {
    oss << "Starting Fixman Mid-Point algorithm at step " << i+1 << "...\n"
        << "There are totally " << NP << " points!";
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Compute the "disturbed" particle velocity + "undisturbed" velocity = U0
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    oss <<"Compute the disturbed particle velocity at step "<<i+1;
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    
    if(i>0){
      *(pm_system.solution) = *v0_ptr; // re-assign the undisturbed solution
      pm_system.update();
      pm_system.reinit_system();
    }
    std::vector<Real> vel0 = pm_system.compute_point_velocity("undisturbed");
    reinit_stokes = false;
    pm_system.solve_stokes("disturbed",reinit_stokes); // Using StokesSolver
    std::vector<Real> vel1 = pm_system.compute_point_velocity("disturbed");
    for(std::size_t j=0; j<vel1.size();++j) vel1[j] += vel0[j];
    brownian_sys.vector_transform(vel1, &U0, "forward");
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     * ---> test: output the particle velocity
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
//    const Real v0_min = v0_ptr->min();
//    const Real v0_max = v0_ptr->max();
//    const Real v0_sum = v0_ptr->sum();
//    if(comm_in.rank()==0){
//      for(unsigned int j=0; j<NP;++j)
//      {
//        std::vector<Real> vtest0(dim), vtest1(dim);
//        for(std::size_t k=0; k<dim;++k){
//          vtest0[k] = vel0[j*dim+k];
//          vtest1[k] = vel1[j*dim+k];
//        }
//        printf("--->test in test_move_particles(): velocity on the %u-th point:\n",j);
//        printf("            U0 = (%f,%f,%f)\n",   vtest0[0],vtest0[1],vtest0[2]);
//        printf("       U0 + U1 = (%f,%f,%f)\n\n", vtest1[0],vtest1[1],vtest1[2]);
//      }
//      printf("            v0_min = %f, v0_max = %f, v0_sum = %f)\n",v0_min,v0_max,v0_sum);
//    }
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     write out the equation systems
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    oss << "Write out particle-mesh equation system into the local file at step "
        << i+1 << ",\n time = " <<real_time;
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    if(i%write_interval==0) o_step++;
    if( write_es && (i%write_interval==0) )
    {
      pm_system.add_local_solution();  // add local solution for the disturbed system
      pm_system.solution->add(*v0_ptr);// add the undisturbed solution
#ifdef LIBMESH_HAVE_EXODUS_API
      exodus_ptr->append(true);
      exodus_ptr->write_timestep(out_filename+".e",pm_system.get_equation_systems(),o_step,o_step);
#endif
    }   // end if( write_es )
    
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     FIXME: Adaptive time step.
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    Real dt = dt0;    // inital time step
    Real vp_max = 0.0, vp_min = 0.0;
    for(unsigned int k=0; k<dim;++k) {
      vp_max += vel1[k]*vel1[k];
    }
    vp_min = vp_max;
    for(unsigned int j=1; j<NP;++j)
    {
      Real vp_norm = 0.0;
      for(std::size_t k=0; k<dim;++k) vp_norm += vel1[j*dim+k]*vel1[j*dim+k];
      vp_max = std::max(vp_max,vp_norm);
      vp_min = std::min(vp_min,vp_norm);
//      oss << "velocity magnitude of the bead "<<j<<" is "<<std::sqrt(vp_norm);
//      PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    }
    vp_max = std::sqrt(vp_max);     // maximum magnitude of particle velocity
    vp_min = std::sqrt(vp_min);
    if(with_brownian) {
      if(vp_max>1.0) dt /= vp_max;  // modify time step(non-dimensional bead radius = 1)
    }
    else {
      const Real aa = 0.05;   // maximum relative deformation of the stiff spring
      if(vp_max*dt>aa*hmins) dt = aa*hmins/vp_max;
      //if(vp_max>hmins) dt *= hmins/vp_max;
    }
    oss << "Max velocity magnitude is " << vp_max << ", hmin_fluid = " << hminf << "\n"
        << "Min velocity magnitude is " << vp_min << ", hmin_solid = " << hmins << "\n"
        << "The time increment at step "<< i+1 << " is dt = " << dt;
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Brownian displacement
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    if (with_brownian)
    {
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       Generate random vector dw whose mean = 0, variance = sqrt(2*dt)
       petsc_random_vector generates a uniform distribution [0 1] whose
       mean = 0.5 and variance = 1/12, so we need a shift and scale operation.
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      oss << "Generate random vector dw at step " << i+1;
      PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
      Real mean_dw = 0.0, variance_dw = 0.0;
      
      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//      // Generate a uniform random vector ranging from 0 to 1 whose mean is close to 0.5.
//      //brownian_sys.petsc_random_vector(n_vec,&rand_ctx,&dw);
//      brownian_sys.std_random_vector(0.0,1.0,"uniform",&dw); // [0 1]
//      brownian_sys._vector_mean_variance(dw, mean_dw, variance_dw);
//      while ( std::abs(mean_dw-0.5)>=0.1 ) {
//        //brownian_sys.petsc_random_vector(n_vec,&rand_ctx,&dw);
//        brownian_sys.std_random_vector(0.0,1.0,"uniform",&dw);
//        brownian_sys._vector_mean_variance(dw, mean_dw, variance_dw);
//      }
//      
//      // the mean is re-adjusted to 0 by substracting 0.5, and the variance by 12
//      VecShift(dw, -0.5);
//      const PetscScalar variance  = std::sqrt(2.0*dt*12.0);
//      VecScale(dw,variance);
      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      
      
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       A more precise way is to construct a random vector with gaussian distribution
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      const Real std_dev  = std::sqrt(dt);
      brownian_sys.std_random_vector(0.0,std_dev,"gaussian",&dw);
      brownian_sys._vector_mean_variance(dw, mean_dw, variance_dw);
      VecScale(dw,std::sqrt(2.0));
      
      
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       TEST: print out the mean and variance or view the generated vector.
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      PetscPrintf(PETSC_COMM_WORLD,
                  "--->test for     random_vector:        mean = %f, variance = %f\n",
                  mean_dw, variance_dw);
      PetscPrintf(PETSC_COMM_WORLD,
                  "Exact values for uniform distribution: mean = %f, variance = %f\n",
                  0.5, 1./12.);
      //PetscPrintf(PETSC_COMM_WORLD,"--->test random vector variance = %f, dw = \n",variance);
      //VecView(dw,PETSC_VIEWER_STDOUT_WORLD);  // View the random vector
      
      
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       Compute dw = B^-1 * dw using Chebyshev polynomial, dw will be changed!
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      oss << "Compute the mid-point Brownian displacement B^-1*dw at step " << i+1;
      PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
      VecCopy (dw,dw_mid);  // save dw to dw_mid, which will be used for Chebyshev
      for(std::size_t j=0; j<2; j++)
      {
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
         Compute the max/min eigenvalues if needed. Otherwise, magnify the interval.
         - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        if(compute_eigen){
          oss << "Compute the max & min eigenvalues for Chebyshev polynomial at step "<<i+1;
          PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
          brownian_sys.compute_eigenvalues(eig_min,eig_max,tol_eigen);
        }
        
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
         Compute the Brownian displacement B^-1 * dw using Chebyshev approximation.
         Here dw is both input and output variables, so it will be changed.
         - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        PetscPrintf(PETSC_COMM_WORLD,
                    "--->eig_min = %f, eig_max = %f, tol_cheb = %f, n_cheb = %d\n",
                    eig_min,eig_max,tol_cheb,n_cheb);
        cheb_converge = brownian_sys.chebyshev_polynomial_approximation(n_cheb,
                                                                        eig_min,eig_max,tol_cheb,&dw);
        
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
         If converged, dw returns the Brownian displacement, then break the j-loop;
         Otherwise, recompute eigenvalues
         - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        if(cheb_converge){
          compute_eigen = false; break;
        }
        else{
          compute_eigen = true;
          VecCopy(dw_mid,dw); /*copy back, recompute eigenvalues*/
          oss << "It is necessry to re-compute the eigenvalues at step " <<i+1;
          PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
        }
      } // end for j-loop
      
      
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       Double-check the convergence of Chebyshev polynomial approximation
       *** If cheb does NOT converge, consider re-generating a rand vector!
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      if(!cheb_converge)
      {
        oss << "****** Warning: Chebysheve failed to converge at step " <<i+1;
        PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
      }
      
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       magnify the spectral range by a factor (1.05 by default), and dw = dw*variance.
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      eig_max *= eig_factor; eig_min /= eig_factor;
      
      
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       Compute dw_mid = D*B^-1*dw, which can be obtained by solving the Stokes
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      oss << "Compute the mid-point Brownian displacement D*B^-1*dw at step " <<i+1;
      PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
      brownian_sys.hi_ewald(M,dw,dw_mid);  // dw_mid = D * dw
    } // end if (with_brownian)
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Particle coordinate vector R0.
     Move the particle R_mid = R0 + 0.5*(U0+U1)*dt (deterministic)
     and R_mid = R_mid + 0.5*sqrt(2)*D*B^-1*dw     (stochastic)
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    oss << "Update the mid-point coordinates at step " <<i+1;
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    brownian_sys.extract_particle_vector(&R0,"coordinate","extract");
    VecWAXPY(R_mid,0.5*dt,U0,R0);  // R_mid = R0 + 0.5*dt*(U0+U1)  (R0 and U0 do NOT change)
    if(with_brownian)
    {
      coef = 0.5;                    // coefficient. sqrt(2) is introduced when generating dw
      VecAXPY(R_mid,coef,dw_mid);    // R_mid = R_mid + 0.5*sqrt(2)*D*B^-1*dw
    }
    brownian_sys.extract_particle_vector(&R_mid,"coordinate","assign"); // Update mid-point coords
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Check and correct the beads' position at the midpoint
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    force_field.check_walls();
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Update the particle mesh for the mid-point step,
     and recompute U0 + U1_mid, D_mid*(B^-1*dw)
     NOTE: the FEM solution of undisturbed field doesn't change, but particles
     move, so U0 needs to be re-evaluated at the new position.
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    oss << "Compute the mid-point particle velocity at step " <<i+1;
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    *(pm_system.solution) = *v0_ptr;       // re-assign the undisturbed solution
    pm_system.update();
    pm_system.reinit_system();
    vel0 = pm_system.compute_point_velocity("undisturbed");
    reinit_stokes = false;
    pm_system.solve_stokes("disturbed",reinit_stokes);   // solve the disturbed solution
    vel1 = pm_system.compute_point_velocity("disturbed");
    for(std::size_t j=0; j<vel1.size();++j) vel1[j] += vel0[j];
    brownian_sys.vector_transform(vel1, &U0, "forward"); // (U0+U1)_mid
    if(with_brownian){
      brownian_sys.hi_ewald(M,dw,dw_mid);  // dw_mid = D_mid*dw, where dw=B^-1*dw computed above
    }
    
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     the mid-point to the NEW point, and update the particle coordinates
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    oss << "Update from mid-point to the NEW particle coordinates at step " <<i+1;
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    VecWAXPY(R_mid,dt,U0,R0);         // R_mid = R0 + dt*U0_mid
    if(with_brownian){
      VecAXPY(R_mid,2.0*coef,dw_mid); // R_mid = R_mid + sqrt(2)*D_mid*B^-1*dw
    }
    brownian_sys.extract_particle_vector(&R_mid,"coordinate","assign");
    
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Check and correct the beads' position again after the midpoint update
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    force_field.check_walls();
    particle_mesh.volume_conservation(mesh_type);
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     update the time
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    real_time += dt;
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     print out particle info if needed. If number of particles is large, we don't print.
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    if (NP<10)
    {
      oss << "Print the particle information at step " <<i+1;
      PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
      if (comm_in.rank() == 0)
      {
        point_mesh.print_point_info();
        //point_mesh.print_elem_neighbor_list();
      }
    }
    comm_in.barrier();
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     write out the particles's surface mesh at the i-th step
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    if(i%write_interval==0)
    {
      if(particle_type=="rigid_particle"){
        std::vector<Real> nodal_vector(n_vec);
        brownian_sys.extract_particle_vector(&R0,"coordinate","extract");
        brownian_sys.vector_transform(nodal_vector,&R0,"backward"); // R0 -> nodal_vector
        
        // write out mesh of all particles
        std::ostringstream smesh_file_name_i;
        smesh_file_name_i << smesh_file_name << "-s."
                          << std::setw(8) << std::setfill('0') << std::right << o_step;
        particle_mesh.update_mesh(nodal_vector);
        particle_mesh.write_particle_mesh(smesh_file_name_i.str());
      }
      
      /* Write CSV file */
//      oss << "points.csv." << o_step;
//      std::string csv_file_name = oss.str();
//      pm_system.write_point_csv(csv_file_name,&U0,false); // write_velocity = false
//      oss.str(""); oss.clear();
    }
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Compute and output mean square displacement at the i-th step
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    VecAXPY(ROUT,dt,U0);              // ROUT = ROUT + dt*U0_mid
    if(with_brownian){
      VecAXPY(ROUT,2.0*coef,dw_mid);  // ROUT = ROUT + sqrt(2)*D_mid*B^-1*dw
    }
    
    /* Output mean square displacement at step i */
    const Real msd = brownian_sys.mean_square_displacement(RIN,ROUT,dim);
    Rg      = brownian_sys.radius_of_gyration(ROUT,dim);
    chain_S = brownian_sys.chain_stretch(ROUT,dim); //(stretch)
    if(comm_in.rank()==0)
    {
      out_msd.open("msd.txt",std::ios_base::app);
      out_msd.setf(std::ios::right);    out_msd.setf(std::ios::fixed);
      out_msd.precision(o_precision);   out_msd.width(o_width);
      out_msd << i+1 << " " << real_time << " " << msd << " " << Rg << " "; // step | time | msd | Rg
      out_msd << chain_S(0) << " " << chain_S(1) << " " << chain_S(2) << "\n";// Sx | Sy | Sz
      out_msd.close();
    }
    
  } // end for i-loop
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Destroy and return
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  oss << "The simulation is finished and Destroy all the PETSc objects.";
  PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
  MatDestroy(&M);
  VecDestroy(&U0);
  VecDestroy(&R0);
  VecDestroy(&R_mid);
  VecDestroy(&dw_mid);
  PetscRandomDestroy(&rand_ctx);
  if(with_brownian){
    VecDestroy(&dw);
  }
  if(exodus_ptr) {
    delete exodus_ptr;
  }
  
  
  // return
  return 0;
}

#endif /* test_sedimentation_01_h */
