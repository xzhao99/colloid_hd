# ---------------------------------------------------------------------------------
# ------------------------- Stokes Solver parameters ------------------------------
# ---------------------------------------------------------------------------------

# Running mode:
restart      = false
restart_step = 10
restart_time = 1.438907510
random_seed  = 123456789


# max linear iterations for the iterative solver
max_linear_iterations = 300


# linear solver tolerence, defaultly = TOLERENCE(1E-6)
linear_solver_rtol = 1E-6
linear_solver_atol = 1E-6


# user defined preconditioning matrix
user_defined_pc = true


# define the type of StokesSolver: field_split; superLU_dist
stokes_solver = superLU_dist


# user defined KSP for schur complement
schur_user_ksp = true
schur_user_ksp_rtol = 1E-6
schur_user_ksp_atol = 1E-6


# schur complement preconditioning type
# SMp: use the pressure mass matrix (***Recommended***)
# SMp_lump:  lumped pressure mass matrix
# S1:   Gt*Kd_inv*G
# S2:   Diag(S1)
# S3:   S3 = Gt*Kd_inv*G (element-wise)
# -------------------- below are NOT implemented --------------------
# S4:   S3_inv
# BFBt and scaled BFBt
# SParse Approximate Commutator(SPAC) and diag scaling SPAC
#
# *warning* If direct method is used to apply BC, symmetry will not be preserved
# Therefore, S1 and S2 can not be used together with -fieldsplit_1_ksp cg.
# In addition, we found that SMp become mesh size dependent, the # of iterations
# decrease as the mesh becomes finer! (Try finer mesh)
schur_pc_type = SMp


# ---------------------------------------------------------------------------------
# -------------------------------- Mesh parameters --------------------------------
# ---------------------------------------------------------------------------------

# Mesh dimensionality
dimension = 3


# size of simulation domain and mesh division
# ------------- microfluidic channel dimension --------------
# random pts are generated within [-10, +10], we here use
# larger domain [-15,+15] to avoid slow convergence of the
# iterative solver!
# -----------------------------------------------------------
XA = -50.
XB = +50.
YA = -50.
YB = +50.
ZA = -50.
ZB = +50.
nx_mesh = 6
ny_mesh = 6
nz_mesh = 6


#XA = -400.
#XB = +400.
#YA = -80.
#YB = +80.
#ZA = -80.
#ZB = +80.
#nx_mesh = 40
#ny_mesh = 8
#nz_mesh = 8


# smoothing parameter in GGEM
alpha = 0.35


# ---------------------------------------------------------------------------------
# ------------------------------ Physical parameters ------------------------------
# ---------------------------------------------------------------------------------

# viscosity. Here we use the unit (cP)
# The visocity of water is
# 1 cP = 1 mPa·s = 0.001 Pa·s = 0.001 N·s·m−2 = 1E-15 N*s/um^2
viscosity = 43.3E-15


# Number of springs per chain => Number of beads then is Nb = Ns+1
Ns = 20


# Kuhn length (um)
bk  = 0.106


# number of Kuhn length per spring
Nks = 19.8


# radius of the bead (um)
radius = 0.077


# Excluded volume parameter (um^3)
ev = 0.0012
