//
//  test_polymer.h
//  
//
//  Created by Xujun Zhao on 1/19/16.
//
//

#ifndef test_polymer_01_h
#define test_polymer_01_h


#include <fstream>

#include "libmesh/getpot.h"
#include "libmesh/mesh.h"
#include "libmesh/serial_mesh.h"
#include "libmesh/mesh_generation.h"
#include "libmesh/mesh_modification.h"
#include "libmesh/mesh_refinement.h"

#include "libmesh/gmv_io.h"
#include "libmesh/exodusII_io.h"

#include "libmesh/dof_map.h"
#include "libmesh/linear_solver.h"
#include "libmesh/equation_systems.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/periodic_boundary.h"
#include "libmesh/sparse_matrix.h"
#include "libmesh/numeric_vector.h"


// include user defined classes or functions
#include "point_particle.h"
#include "particle_mesh.h"
#include "point_mesh.h"
#include "force_field.h"
#include "pm_linear_implicit_system.h"
#include "brownian_system.h"
#include "pm_periodic_boundary.h"
#include "chebyshev.h"
#include "pm_toolbox.h"
#include "polymer_chain.h"
#include "random_generator.h"
#include "stokes_solver.h"



/*
 * This example models DNA chain passing through a nanochannel.
 */


int test_polymer_01(const Parallel::Communicator &comm_in)
{
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### running test_polymer_01\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Define constants:
   kB = 1.380662E-23(J/K) = 1.380662E-23(N*m/K) = 1.380662E-17 (N*um/K)
   T  = 297 K
   ===> kBT = 4.1E-15 (N*um)
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  const Real PI     = libMesh::pi;
  const Real kBT    = 4.1E-15;     // 
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Parse the input file and read in parameters from the input file
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  GetPot input_file("polymer_control.in");
  const bool  restart       = input_file("restart", false);
  std::size_t restart_step  = input_file("restart_step", 0);
  std::size_t random_seed   = input_file("random_seed",111);
  Real        restart_time  = input_file("restart_time", 0.0);
  if(restart) // update the seed for restart mode
  {
    random_seed++;
  }
  else        // set the restart_step as zero
  {
    restart_step = 0;
    restart_time = 0.0;
  }
  
  
  // (1) Stokes solver parameters
  const int max_linear_iterations = input_file("max_linear_iterations", 100);
  const Real linear_solver_rtol   = input_file("linear_solver_rtol", 1E-6);
  const Real linear_solver_atol   = input_file("linear_solver_atol", 1E-6);
  bool user_defined_pc            = input_file("user_defined_pc", true);
  const bool schur_user_ksp       = input_file("schur_user_ksp", false);
  const Real schur_user_ksp_rtol  = input_file("schur_user_ksp_rtol", 1E-6);
  const Real schur_user_ksp_atol  = input_file("schur_user_ksp_atol", 1E-6);
  const std::string schur_pc_type = input_file("schur_pc_type", "SMp");
  const std::string stokes_solver_type = input_file("stokes_solver", "superLU_dist");
  StokesSolverType solver_type;
  if(stokes_solver_type=="superLU_dist") {
    solver_type = superLU_dist;
    user_defined_pc = false;
  }
  else if(stokes_solver_type=="field_split") {
    solver_type = field_split;
    user_defined_pc = true;
  }
  else {
    solver_type = user_define;
  }
  
  
  // (2) Mesh & Geometry parameters
  const unsigned int dim          = input_file("dimension", 3);
  const Real XA                   = input_file("XA", -1.);
  const Real XB                   = input_file("XB", +1.);
  const Real YA                   = input_file("YA", -1.);
  const Real YB                   = input_file("YB", +1.);
  Real ZA                         = input_file("ZA", -1.);
  Real ZB                         = input_file("ZB", +1.);
  if (dim==2){  ZA = 0.;  ZB = 0.;  }
  const unsigned int nx_mesh      = input_file("nx_mesh", 20);
  const unsigned int ny_mesh      = input_file("ny_mesh", 10);
  const unsigned int nz_mesh      = input_file("nz_mesh", 10);
  const Real alpha                = input_file("alpha", 0.1);
  
  
  // (3) Physical parameters of polymer and fluids
  const Real viscosity            = input_file("viscosity", 1.0); // viscosity (cP = N*s/um^2)
  const unsigned int Ns           = input_file("Ns", 20);   // total # of springs
  const Real bk                   = input_file("bk", 1E-6); // Kuhn length (um)
  const Real Nks                  = input_file("Nks",1E-6); // # of Kuhn length per spring
  const Real Rb                   = input_file("radius", 0.10); // radius of the bead (um)
  const Real ev                   = input_file("ev", 1E-3); // excluded volume parameter
  
  
  // (4) Compute other parameters using the input
  Real min_mesh_size, max_mesh_size;
  const Real meshsize_x   = (XB - XA)/Real( nx_mesh );
  const Real meshsize_y   = (YB - YA)/Real( ny_mesh );
  const Real meshsize_z   = (ZB - ZA)/Real( nz_mesh );
  min_mesh_size           = std::min(meshsize_x, meshsize_y);
  min_mesh_size           = std::min(min_mesh_size, meshsize_z);
  max_mesh_size           = std::max(meshsize_x, meshsize_y);
  max_mesh_size           = std::max(min_mesh_size, meshsize_z);
  
  
  const Real  drag_c      = 6.*PI*viscosity*Rb;    // Drag coefficient (N*s/um)
  const Real  q0          = Nks*bk;         // Maximum spring length (um)
  const Real chain_length = Ns*q0;          // contour length of the chain (um)
  const unsigned int Nb   = Ns + 1;         // # of beads
  const Real  Db          = kBT/drag_c;     // diffusivity of a bead (um^2/s)
  const Real  Dc          = Db/Real(Nb);    // diffusivity of the chain (um^2/s)
  const Real  Ss2         = Nks*bk*bk/6.;   // (um^2)
  //const Real  Ls0  = 0.9*q0/bead_r;         // initial spring lenght (dimensionless).
  
  // (5) characteristic variables
  const Real tc   = drag_c*Rb*Rb/kBT;       // diffusion time (s)
  const Real uc   = kBT/(drag_c*Rb);        // characteristic velocity (um/s)
  const Real fc   = kBT/Rb;                 // characteristic force (N)
  const Real muc  = 1./(6.*PI);             // non-dimensional viscosity
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * The larger the alpha, the sharper the g function, and the more singular the solution.
   * which requires finer mesh to resolve the force function g.
   * When alpha->inf, the modified Gaussian force g -> 3D delta function.
   
   * If we choose small alpha, the force function g becomes smooth, then can use coarsen mesh.
   * However, g will decay slowly, and a larger neigbhor list should be built in GGEM.
   * Therefore,  search_radius ~ 1/alpha
   
   * Usually take h <= 1/sqrt(2)/alpha: e.g. if alpha=0.1 => h<7.1
   * When alpha is too small, g function is very smooth and not vanishes at the domain bdry
   * Therefore, int[g(x)] over the domain is not equal to 1, the results are inaccurate!
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Output the selected parameters on the screen
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  std::cout << "\n\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The mesh and geometric info:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "nx_mesh = " << nx_mesh <<", Lx = " << XB-XA <<", hx = "<< meshsize_x <<"\n"
            << "ny_mesh = " << ny_mesh <<", Ly = " << YB-YA <<", hy = "<< meshsize_y <<"\n"
            << "nz_mesh = " << nz_mesh <<", Lz = " << ZB-ZA <<", hz = "<< meshsize_z <<"\n"
            << "minimum mesh size of fluid: hmin = " << min_mesh_size << "\n"
            << "the smoothing parameter in GGEM alpha = " << alpha << "\n";
  std::cout << std::endl;
  
  
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The Stokes solver info:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "Stokes solver type = " << stokes_solver_type << std::endl;
  if (stokes_solver_type=="field_split")
  {
    std::cout << "FieldSplit Schur Complement Reduction Solver\n";
    std::cout << "schur_pc_type = " << schur_pc_type << std::endl;
    if(schur_user_ksp)
    {
      std::cout<<"user defined KSP is used for Schur Complement!"<< std::endl;
      std::cout<<"KSP rel tolerance for Schur Complement solver is = " << schur_user_ksp_rtol <<"\n";
      std::cout<<"KSP abs tolerance for Schur Complement solver is = " << schur_user_ksp_atol <<"\n";
    }
  }
  std::cout << "\n\n";
  
  
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The physical parameters in the simulation:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "   viscosity                         mu  = " << viscosity <<" (cP = N*s/um^2)\n";
  std::cout << "                                     kBT = " << kBT << " (N*um = N*um)\n";
  std::cout << "   number of springs                 Ns  = " << Ns << "\n";
  std::cout << "   number of beads                   Nb  = " << Nb << "\n";
  std::cout << "   Kuhn length                       bk  = " << bk << " (um)\n";
  std::cout << "   # of Kuhn segment per spring      Nks = " << Nks << "\n";
  std::cout << "   Radius of the bead                a   = " << Rb << " (um)\n";
  std::cout << "   maximum spring length             q0  = " << q0 << " (um)\n";
  std::cout << "   chain length of polymer           Lc  = " << chain_length << " (um)\n";
  std::cout << "   bead diffusivity                  Db  = " << Db << " (um^2/s)\n";
  std::cout << "   chain diffusivity                 Dc  = " << Dc << " (um^2/s)\n";
  std::cout << "                                     Ss2 = " << Ss2 << " (um^2)\n";
  std::cout << "   HI Drag coefficient   zeta = 6*PI*mu*a = " << drag_c << " (N*s/um)\n";
  std::cout << "                      ksi = sqrt(PI)/(3a) = " << std::sqrt(PI)/(3.*Rb) <<"\n";
  std::cout << "   Excluded volume parameter          ev  = " << ev << " (um^3)\n";
  std::cout << "\n\n";
  
  
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The characteristic variables:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "   characteristic time          = " << tc << " (s)\n";
  std::cout << "   characteristic velocity      = " << uc << " (m/s)\n";
  std::cout << "   characteristic force         = " << fc << " (N)\n";
  std::cout << "\n\n";
  
  
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The non-dimensional variables:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "   non-dimensional bead radius      a0     = " << 1.0 << "\n";
  std::cout << "   non-dimensional Kuhn length    bk/a     = " << bk/Rb << "\n";
  std::cout << "   non-dimensional spring length  q0/a     = " << q0/Rb << "\n";
  std::cout << "   non-dimensional contour length Lc/a     = " << chain_length/Rb << "\n";
  std::cout << "   non-dimensional Ss/a = sqrt(Ss2/a^2)    = " << std::sqrt(Ss2/Rb/Rb) << "\n";
  std::cout << "   non-dimensional ksi = sqrt(PI)/(3a0)    = " << std::sqrt(PI)/(3.) << "\n";
  std::cout << "\n\n";
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * Create a mesh, distributed across the default MPI communicator.
   * We build a mesh with Quad9(8) elements for 2D and HEX27(20) element for 3D
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### Create finite element mesh:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  SerialMesh mesh(comm_in);   //Mesh mesh(comm_in);
  if(dim==2)
    MeshTools::Generation::build_square (mesh, nx_mesh, ny_mesh,
                                         XA, XB, YA, YB, QUAD8);        // QUAD8/9
  else if(dim==3)
    MeshTools::Generation::build_cube (mesh, nx_mesh, ny_mesh, nz_mesh,
                                       XA, XB, YA, YB, ZA, ZB, HEX20);  // HEX20/27
  else
    libmesh_example_requires(dim <= LIBMESH_DIM, "2D/3D support");
  // end if
  mesh.print_info();
  std::cout << "\n\n";
  //mesh.get_boundary_info().print_info();
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * - - - - - - - - - - - - - - TEST: random generator - - - - - - - - - - - - - -
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
//  RandomGenerator random_generator;
//  const std::size_t n_rand = 20;
//  const Real mean_val = 5.;
//  const Real dev_val  = 2.;
//  std::vector<Real> rand_vec0,rand_vec1,rand_vec2;
//  
//  // normal(Gaussian distribution)
////  rand_vec0 = random_generator.random_vector_normal(n_rand,mean_val,dev_val);
////  rand_vec1 = random_generator.random_vector_normal(n_rand,mean_val,dev_val);
////  rand_vec2 = random_generator.random_vector_normal(n_rand,mean_val,dev_val);
//  
//  // uniform distribution [0 1]
//  //random_generator.set_seed(1235689365);
//  rand_vec0 = random_generator.random_vector_uniform(n_rand,0,1);
//  rand_vec1 = random_generator.random_vector_uniform(n_rand,0,1);
//  rand_vec2 = random_generator.random_vector_uniform(n_rand,0,1);
//  
//  // Check if the engine generate different random numbers every time.(YES)
//  std::cout<<  "                " <<"  v0  "<<"    v1    "<<"    v2  "<<"\n";
//  for(std::size_t i=0; i<rand_vec0.size(); ++i){
//    std::cout<<"rand number "<<i<<" = "
//    <<rand_vec0[i]<<",  "<<rand_vec1[i]<<",  "<<rand_vec2[i]<<"\n";
//  }
//  std::cout<<"\n\n";
//  
//  
//  // use printf to check if the random values on each processor are the same(YES)
//  if(comm_in.rank()==0){
//    printf("Random vector 0 on the Processor 0:\n");
//    for(std::size_t i=0; i<rand_vec0.size(); ++i){
//      printf("rand number %lu = %f\n",i,rand_vec0[i]);
//    }
//    printf("\n");
//  }
//  comm_in.barrier();
//  if(comm_in.rank()==1){
//    printf("Random vector 0 on the Processor 1:\n");
//    for(std::size_t i=0; i<rand_vec0.size(); ++i){
//      printf("rand number %lu = %f\n",i,rand_vec0[i]);
//    }
//    printf("\n");
//  }
//  comm_in.barrier();
//  if(comm_in.rank()==2){
//    printf("Random vector 0 on the Processor 2:\n");
//    for(std::size_t i=0; i<rand_vec0.size(); ++i){
//      printf("rand number %lu = %f\n",i,rand_vec0[i]);
//    }
//    printf("\n");
//  }
//  
//  return 0;
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Test the ParticleMesh/PointMesh class
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### Create periodic box, Polymer chain(s) and point-mesh:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  
  // add particle-mesh periodic boundary in the x-direction
  const Point bbox_pmin(XA, YA, ZA);
  const Point bbox_pmax(XB, YB, ZB);
  std::vector<bool> periodic_direction(dim,false);
//  periodic_direction[0] = true; // periodic boundary in x-direction
//  periodic_direction[1] = true; // periodic boundary in y-direction
//  periodic_direction[2] = true; // periodic boundary in z-direction
  PMPeriodicBoundary pm_periodicity(bbox_pmin, bbox_pmax, periodic_direction);
  
  
  // Read the polymer data from the local input file
  const unsigned int chain_id = 0;
  PolymerChain polymer_chain(chain_id);
  std::ostringstream pfilename;
  if(restart)
  {
    pfilename << "output_polymer_" << restart_step << ".vtk";
    polymer_chain.read_data_vtk(pfilename.str());
    if(comm_in.rank()==0) polymer_chain.write_polymer_chain("output_test_polymer_restart.vtk");
  }
  else
  {
    pfilename << "polymer_data.in";
    //polymer_chain.read_data(pfilename);
    polymer_chain.read_data_pizza(pfilename.str());
    if(comm_in.rank()==0) polymer_chain.write_polymer_chain("output_test_polymer0.vtk");
  }
  pfilename.str(""); pfilename.clear();
  comm_in.barrier();
  
  
  // -------------------------------- TEST --------------------------------
//  const Point pt(10,0,0);
//  std::vector<Real> rot_vec(4);
//  polymer_chain.add_bead(pt,0,rot_vec);
//  if(comm_in.rank()==0) polymer_chain.write_polymer_chain("output_test_polymer.vtk");
//  if(comm_in.rank()==0){
//    polymer_chain.print_info();
//    //const Real p_len = polymer_chain.compute_chain_length();
//    //printf("--->TEST: the computed polymer chain length is %f\n\n", p_len);
//  }
  
  // test read vtk files
//  polymer_chain.read_data_vtk("output_test_polymer.vtk");
//  if(comm_in.rank()==0) polymer_chain.write_polymer_chain("output_test_polymer_1.vtk");
//  return 0;
  // -----------------------------------------------------------------------
  
  
  // Construct PointMesh object from the polymer
  const Real search_radius_p = 4.0/alpha;
  const Real search_radius_e = 0.5*max_mesh_size + search_radius_p;
  PointMesh<3> point_mesh(mesh, polymer_chain, search_radius_p, search_radius_e);
  point_mesh.add_periodic_boundary(pm_periodicity);
  point_mesh.reinit();

  
  
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "### The point-mesh info:\n";
  std::cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
  std::cout << "Total number of particles: " << point_mesh.num_particles() << "\n";
  std::cout <<"search_radius_p = "<<search_radius_p <<", search_radius_e = "<<search_radius_e<<"\n";
  std::cout <<"Periodic BC in x-y-z directions: ";
  for (std::size_t i=0; i<dim; ++i)
  {
    if( periodic_direction[i] )
    {
      std::cout<<"  TRUE";
    }
    else
    {
      std::cout<<"  FALSE";
    }
  }
  std::cout << "\n\n\n";
  
//  if(comm_in.rank()==0)
//  {
//    point_mesh.print_point_info();
//    point_mesh.print_elem_neighbor_list();
//  }
//  return 0;
//  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Create an equation systems and ParticleMeshLinearImplicitSystem "Stokes",
   and add variables: velocity (u, v, w) and pressure p. To satisfy LBB condition,
   (u, v, w):  second-order approximation
   p :         first-order basis
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  EquationSystems equation_systems (mesh);
  PMLinearImplicitSystem& system = equation_systems.add_system<PMLinearImplicitSystem> ("Stokes");
  unsigned int u_var = 0, v_var = 0, w_var = 0;
  u_var = system.add_variable ("u", SECOND);
  v_var = system.add_variable ("v", SECOND);
  if(dim==3)  w_var  = system.add_variable ("w", SECOND);
  const unsigned int p_var = system.add_variable ("p", FIRST);
  
  /* attach the particle-mesh system */
  system.attach_point_mesh(&point_mesh);

  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Add periodic boundary conditions for the system.
   For the side number of a box, refer to libMesh::Hex27::build_side()
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  DofMap& dof_map = system.get_dof_map();
  
  /*** set PBC in x-direction ***/
  if (periodic_direction[0])
  {
    PeriodicBoundary pbcx(RealVectorValue(XB-XA, 0., 0.));
    pbcx.set_variable(u_var);
    pbcx.set_variable(v_var);
    if(dim==3) pbcx.set_variable(w_var);
    //pbcx.set_variable(p_var); //*** NOT include p!
    
    // is this boundary number still true for 3D?
    if(dim==2)
    {
      pbcx.myboundary = 3;
      pbcx.pairedboundary = 1;
    }
    else if(dim==3)
    {
      pbcx.myboundary = 4;
      pbcx.pairedboundary = 2;
    } // end if
    
    dof_map.add_periodic_boundary(pbcx);
    
    // check
    if (search_radius_p>=(XB-XA)/2. && comm_in.rank()==0)
    {
      printf("\n\n");
      printf("****************************** warning: ********************************\n");
      printf("**** The search radius is larger than the domain length in x direction! \n");
      printf("**** search radius = %f, domain size Lx = %f\n",search_radius_p,(XB-XA)/2.);
      printf("************************************************************************\n\n\n");
    }
  }
  
  /*** set PBC in y-direction ***/
  if (periodic_direction[1])
  {
    PeriodicBoundary pbcy(RealVectorValue(0., YB-YA, 0.));
    pbcy.set_variable(u_var);
    pbcy.set_variable(v_var);
    if(dim==3) pbcy.set_variable(w_var);
    //pbcy.set_variable(p_var); //*** NOT include p!
    
    if(dim==2)
    {
      pbcy.myboundary = 0;
      pbcy.pairedboundary = 2;
    }
    else if(dim==3)
    {
      pbcy.myboundary = 1;
      pbcy.pairedboundary = 3;
    } // end if
    
    dof_map.add_periodic_boundary(pbcy);
    
    // check
    if (search_radius_p>=(YB-YA)/2. && comm_in.rank()==0)
    {
      printf("\n\n");
      printf("****************************** warning: ********************************\n");
      printf("**** The search radius is larger than the domain length in y direction!\n");
      printf("**** search radius = %f, domain size Ly = %f\n",search_radius_p,(YB-YA)/2.);
      printf("************************************************************************\n\n\n");
    }
  }
  
  
  /*** set PBC in z-direction ***/
  if (periodic_direction[2])
  {
    PeriodicBoundary pbcz(RealVectorValue(0., 0., ZB-ZA));
    pbcz.set_variable(u_var);
    pbcz.set_variable(v_var);
    if(dim==3) pbcz.set_variable(w_var);
    //pbcz.set_variable(p_var); //*** NOT include p!
    
    if(dim==3)
    {
      pbcz.myboundary = 0;
      pbcz.pairedboundary = 5;
    } // end if
    
    dof_map.add_periodic_boundary(pbcz);
    
    // check
    if (search_radius_p>=(ZB-ZA)/2. && comm_in.rank()==0)
    {
      printf("\n\n");
      printf("****************************** warning: ********************************\n");
      printf("**** The search radius is larger than the domain length in z direction!\n");
      printf("**** search radius = %f, domain size Lz = %f\n",search_radius_p,(ZB-ZA)/2.);
      printf("************************************************************************\n\n\n");
    }
  }
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Initialize the Preconditioning matrix for saddle point problems if required.
   Initialize the equation system and zero the preconditioning matrix
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if( user_defined_pc ) system.add_matrix("Preconditioner");
  
  /* Initialize the data structures for the equation system. */
  equation_systems.init ();
  
  // zero the PC matrix, which MUST be done after es.init()
  if( user_defined_pc ) system.get_matrix("Preconditioner").zero();
  
  std::cout<<"###Equation systems are initialized:\n"<<std::endl;
  

  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   set parameters of equations systems
   * NOTE: some of these parameters are not used if we use non-dimensional formulation
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  equation_systems.parameters.set<unsigned int>("linear solver maximum iterations") = max_linear_iterations;
  equation_systems.parameters.set<Real> ("linear solver rtol") = linear_solver_rtol;
  equation_systems.parameters.set<Real> ("linear solver atol") = linear_solver_atol;
  equation_systems.parameters.set<bool>    ("user_defined_pc") = user_defined_pc;
  equation_systems.parameters.set<bool>     ("schur_user_ksp") = schur_user_ksp;
  equation_systems.parameters.set<Real>("schur_user_ksp_rtol") = schur_user_ksp_rtol;
  equation_systems.parameters.set<Real>("schur_user_ksp_atol") = schur_user_ksp_atol;
  equation_systems.parameters.set<std::string>    ("schur_pc_type") = schur_pc_type;
  equation_systems.parameters.set<StokesSolverType> ("solver_type") = solver_type;
  
  equation_systems.parameters.set<Real>        ("XA_boundary") = XA;
  equation_systems.parameters.set<Real>        ("XB_boundary") = XB;
  equation_systems.parameters.set<Real>        ("YA_boundary") = YA;
  equation_systems.parameters.set<Real>        ("YB_boundary") = YB;
  equation_systems.parameters.set<Real>        ("ZA_boundary") = ZA;
  equation_systems.parameters.set<Real>        ("ZB_boundary") = ZB;
  equation_systems.parameters.set<Real>              ("alpha") = alpha;
  equation_systems.parameters.set<Real>   ("fluid mesh size")  = min_mesh_size;
  
  //equation_systems.parameters.set<Real>         ("viscosity")  = viscosity;
  //equation_systems.parameters.set<Real>               ("kBT")  = kBT;
  equation_systems.parameters.set<Real>       ("viscosity_0")  = muc;
  equation_systems.parameters.set<Real>               ("br0")  = 1.0;
  equation_systems.parameters.set<Real>               ("bk")   = bk;
  equation_systems.parameters.set<Real>               ("q0")   = q0;
  equation_systems.parameters.set<Real>       ("bead radius")  = Rb;
  equation_systems.parameters.set<Real>              ("drag")  = drag_c;
  equation_systems.parameters.set<Real>               ("Nks")  = Nks;
  equation_systems.parameters.set<Real>               ("Ss2")  = Ss2;
  equation_systems.parameters.set<Real>                ("ev")  = ev;
  equation_systems.parameters.set<Real>                ("tc")  = tc;
  equation_systems.parameters.set<std::string> ("particle_type")  = "point_particle";
  
  /* Print information about the mesh and system to the screen. */
  //mesh.print_info();
  equation_systems.print_info();
  std::cout <<"  System has: "<< mesh.n_elem()<<" elements,\n"
            <<"              "<< mesh.n_nodes()<<" nodes,\n"
            <<"              "<< equation_systems.n_dofs()<<" degrees of freedom.\n"
            <<"              "<< equation_systems.n_active_dofs()<<" active degrees of freedom.\n"
            <<"              "<< point_mesh.num_particles()<<" particles.\n" << std::endl;
  
//  // write out the particle surface mesh
//  ExodusII_IO(mesh).write_equation_systems("output_test_pm_system.e",equation_systems);
//  return 0;
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Init the force field and attach it to the PMLinearImplicitSystem pm_system
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ForceField force_field(system);
  system.attach_force_field(&force_field);
  
  
  
  /* ------------------------------------------------------------------------------------*/
  /* ---------------------------------- TEST PROGRAM ----------------------------------- */
  /* ------------------------------------------------------------------------------------*/
//  std::string msg = "--->TEST reinit particle mesh:";
//  PMToolBox::output_message(msg,comm_in);
//  system.reinit_system();
//  if(comm_in.rank()==0)
//  {
//    point_mesh.print_point_info();
//    //point_mesh.print_elem_neighbor_list();
//  }
//  
//  msg = "--->TEST Stokes solver with point forces:";
//  PMToolBox::output_message(msg,comm_in);
//  bool re_init_stokes = true;
//  system.solve_stokes("undisturbed",re_init_stokes);
//  ExodusII_IO(mesh).write_equation_systems("output_test_pm_system0.e",equation_systems);
//  
//  re_init_stokes = false;
//  system.solve_stokes("disturbed",re_init_stokes);
//  system.add_local_solution();
//  ExodusII_IO(mesh).write_equation_systems("output_test_pm_system1.e",equation_systems);
//  if(comm_in.rank()==0) polymer_chain.write_polymer_chain( "output_test_polymer.vtk" );
//  return 0;
  
  
  
  
  /* ------------------------------------------------------------------------------------*/
  /* ------------------------------ TEST Brownian System ------------------------------- */
  /* ------------------------------------------------------------------------------------*/
//  std::string msg = "--->TEST Brownian System:";
//  PMToolBox::output_message(msg,comm_in);
//  system.reinit_system();
//  BrownianSystem brownian_sys0(equation_systems);

//  msg = "--->5. test the eigen value solver!";
//  PMToolBox::output_message(msg,comm_in);
//  const Real tol_eigen0 = 0.01;
//  Real eig_min0, eig_max0;  // [ 0.035011, 5.106635 ]
//  brownian_sys0.compute_eigenvalues(eig_min0,eig_max0,tol_eigen0);
  //brownian_sys0.compute_eigenvalue("largest",tol_eigen);
  //brownian_sys0.compute_eigenvalue("smallest",tol_eigen);
  //brownian_sys0.power_iteration();
  
  
  /* -------------------------------------------------------------------
   test the Chebyshev polynomial approximation
   ---------------------------------------------------------------------*/
//  msg = "--->6. test the Chebyshev polynomial approximation!";
//  PMToolBox::output_message(msg,comm_in);

  // (1.1) generate random vector dw
//  Vec dw0;
//  PetscRandom rand_ctx0;
//  const std::size_t N0 = 21*3, n_cheb0 = 50;
//  brownian_sys0.init_petsc_random(&rand_ctx0);
//  for(std::size_t i=0; i<6; ++i)
//  {
//    brownian_sys0.petsc_random_vector(N0,&rand_ctx0,&dw0);
//    PetscPrintf(PETSC_COMM_WORLD,"--->test petsc_random_vector dw%d = \n",i);
//    VecView(dw0,PETSC_VIEWER_STDOUT_WORLD);  // View the random vector
//  }
//  VecShift(dw0, -0.5);
//  PetscPrintf(PETSC_COMM_WORLD,"--->test petsc_random_vector end 1!! \n");
//  const PetscScalar variance = std::sqrt(2.0*0.1*12.0);  // dt = 0.1
//  PetscPrintf(PETSC_COMM_WORLD,"--->test petsc_random_vector end 2!! \n");
//  VecScale(dw0,variance);
//  PetscPrintf(PETSC_COMM_WORLD,"--->test petsc_random_vector end 3!! \n");
  
  
  // (1.2) generate random vector dw using std library
//  Vec dw0;
//  //unsigned int rand_seed = brownian_sys0.random_generator().generator();
//  brownian_sys0.set_std_random_seed(random_seed);
//  std::cout << "Current random seed is " << brownian_sys0.random_generator().generator() << std::endl;
//  for(std::size_t i=0; i<5; ++i)
//  {
//    brownian_sys0.std_random_vector(0.,1.,"uniform",&dw0);
//    PetscPrintf(PETSC_COMM_WORLD,"--->test std_random_vector dw%d = \n",i);
//    //VecView(dw0,PETSC_VIEWER_STDOUT_WORLD);  // View the random vector
//  }
  
//  // (2) test the Chebyshev: B^-1 * dw
//  const Real eig_factor = 1.0, tol_cheb = 1E-3;
//  const Real eig_a = 0.820294/eig_factor; // 0.820294/0.822763
//  const Real eig_b = 1.612086*eig_factor; // 1.612086/1.612328
////  const Real eig_min = 1.0/eig_factor;
////  const Real eig_max = Real(N)*eig_factor;
//  brownian_sys0.chebyshev_polynomial_approximation(n_cheb0,eig_a,eig_b,tol_cheb,&dw0);
////  PetscPrintf(PETSC_COMM_WORLD,"--->test in petsc_random_vector B^-1 * dw = \n");
////  VecView(dw,PETSC_VIEWER_STDOUT_WORLD);  // View the random vector

  // (3) destroy the PETSc objects.
//  VecDestroy(&dw0);
//  PetscRandomDestroy(&rand_ctx0); // Destroy this after vector, otherwise cause errors!
//  PetscPrintf(PETSC_COMM_WORLD,"--->test petsc_random_vector end 4!! \n");
//  return 0;

  
  
  
  
  
  /* ------------------------------------------------------------------------------------
   * ------------------------------ TEST Moving particles -------------------------------
   (1) change mesh file name;
   (2) change BCs
   (3) change bead-wall distance function
   (4) change the check_wall function for different geometries
   (5) change external force acting on the beads.
   (6) change the dw magnification factor to accelerate the convergence (X)
   * ------------------------------------------------------------------------------------*/
  std::string msg6 = "--->TEST Moving point particles:";
  PMToolBox::output_message(msg6,comm_in);
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Collect system parameters for the simulation
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  const unsigned int NP     = point_mesh.num_particles();
  const unsigned int n_vec  = dim*NP;
  const unsigned int n_cheb = 50;
  const Real       tol_cheb = 0.1;
  const Real     eig_factor = 1.05;
  const Real      tol_eigen = 0.01;
  bool  cheb_converge, compute_eigen = true;
  Real eig_min = 0., eig_max = 0., real_time = restart_time;
  
  
  const Real  max_spring_len = q0/Rb;     // non-dimensional max spring length
  const Real          Ss2_a2 = Ss2/Rb/Rb; // dt =  c*Ss2/a2
  const Real            dt0  = 0.1*Ss2_a2;
  const unsigned int  nstep  = 10;
  const unsigned int write_interval = 1;
  const bool  with_brownian = true;

  
  std::ostringstream  oss;
  const bool  write_es   =  true;
  const bool  print_info =  false;
  const Real  hmin = equation_systems.parameters.get<Real>("fluid mesh size");
  const std::string particle_type = equation_systems.parameters.get<std::string>("particle_type");
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Compute undisturbed velocity field without particles.
   NOTE: We MUST re-init particle-mesh before solving Stokes
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  oss << "Computing the undisturbed velocity field ......";
  PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
  system.reinit_system();
  bool reinit_stokes = true;
  system.solve_stokes("undisturbed",reinit_stokes);
  UniquePtr<NumericVector<Real>> v0_ptr = system.solution->clone(); // backup v0
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Print out the particle-mesh information if needed
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  comm_in.barrier();
  if ( print_info && (comm_in.rank() == 0) )
  {
    oss << "Print out point-mesh information at step 0.";
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    point_mesh.print_point_info();
    point_mesh.print_elem_neighbor_list();
  }
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   write out the equation systems if write_es = true at Step 0 (undisturbed field)
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  oss << "Write out particle-mesh system into the local file at step 0.";
  PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
  const std::string out_filename   = "output_pm_system";
  ExodusII_IO*  exodus_ptr = new ExodusII_IO(mesh);
  if(write_es && restart==false)
  {
    //system.add_local_solution(); // Don't add local solution for undisturbed system!
#ifdef LIBMESH_HAVE_EXODUS_API
    exodus_ptr->write_equation_systems(out_filename+".e", equation_systems);
#endif
  }   // end if( write_es )
  
  /* output polymer chain data at the 0-th step in the VTK format */
  if(restart==false)
  {
    oss << "output_polymer_" << 0 << ".vtk";
    if(comm_in.rank()==0) polymer_chain.write_polymer_chain( oss.str() );
    oss.str(""); oss.clear();
  }
  
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Create vectors and Shell Mat for use:
   U0:          particle velocity vector;
   R0/R_mid:    particle position vector;
   dw/dw_mid:   random vector;
   RIN/ROUT:    the initial and intermediate particle postion vector for msd output
   RIN will not change, and ROUT excludes pbc
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  Vec             U0, R0, R_mid, RIN,ROUT, dw, dw_mid;
  Mat             M;
  PetscRandom     rand_ctx;
  PetscViewer     viewer;
  PetscScalar     coef = 0.0;
  BrownianSystem brownian_sys (equation_systems);
  brownian_sys.init_petsc_random(&rand_ctx);
  brownian_sys._create_shell_mat(n_vec, &M);
  brownian_sys._create_petsc_vec(n_vec,&R0);
  VecDuplicate(R0,&U0);
  VecDuplicate(R0,&R_mid);
  VecDuplicate(R0,&dw_mid);
  brownian_sys.extract_particle_vector(&ROUT,"coordinate","extract");
  VecDuplicate(ROUT,&RIN);
  VecCopy(ROUT,RIN);  // RIN = ROUT = the initial position vector
  brownian_sys.set_std_random_seed(random_seed);  // random seed
  
  if(restart)
  {
    // read RIN & ROUT from local file output during the previous simulation
    PetscViewerBinaryOpen(PETSC_COMM_WORLD,"vector_RIN.dat",FILE_MODE_READ,&viewer);
    VecLoad(RIN,viewer);
    PetscViewerBinaryOpen(PETSC_COMM_WORLD,"vector_ROUT.dat",FILE_MODE_READ,&viewer);
    VecLoad(ROUT,viewer);
  }
  else
  {
    // write out binary file of RIN, which may be used at restart mode.
    PetscViewerBinaryOpen(PETSC_COMM_WORLD,"vector_RIN.dat",FILE_MODE_WRITE,&viewer);
    VecView(RIN,viewer);
  }
  comm_in.barrier();
  
  
  /* Output mean square displacement and radius of gyration at step 0 */
  const Point center0 = brownian_sys.center_of_mass(RIN,dim);
  Real  Rg      = brownian_sys.radius_of_gyration(RIN,center0,dim);
  Point chain_S = brownian_sys.chain_stretch(RIN,dim); //(stretch)
  std::ofstream out_msd;
  int o_width = 10, o_precision = 9;
  if(comm_in.rank()==0 && restart==false) // Don't write at restart mode.
  {
    out_msd.open("msd.txt",std::ios_base::out);
    out_msd.setf(std::ios::right);    out_msd.setf(std::ios::fixed);
    out_msd.precision(o_precision);   out_msd.width(o_width);
    out_msd << 0 << " " << 0.0 << " ";                  // step | time |
    out_msd << 0.0 << " " << 0.0 << " " << 0.0 << " " << Rg << " ";  // | msd_x | msd_y | msd_z | Rg
    out_msd << chain_S(0) << " " << chain_S(1) << " " << chain_S(2) << "\n";// Sx | Sy | Sz
    out_msd.close();
  }
//  Real  Rg0      = brownian_sys.radius_of_gyration(ROUT,dim);
//  printf("--->test: Rg0 = %f, Rg = %f\n", Rg0, Rg);
//  return 0;
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Advancing in time. Fixman Mid-Point algorithm
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  const unsigned int istart = restart_step;
  const unsigned int iend   = restart_step + nstep;
  unsigned int o_step = restart_step;  // output step
  for(unsigned int i=istart; i<iend; ++i)
  {
    oss << "Starting Fixman Mid-Point algorithm at step " << i+1 << "...\n"
        << "There are totally " << NP << " points!";
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Compute the "disturbed" particle velocity + "undisturbed" velocity = U0
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    oss <<"Compute the disturbed particle velocity at step "<<i+1;
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    
    if(i>0){
      *(system.solution) = *v0_ptr; // re-assign the undisturbed solution
      system.update();
      system.reinit_system();
    }
    std::vector<Real> vel0 = system.compute_point_velocity("undisturbed");
    reinit_stokes = false;
    system.solve_stokes("disturbed",reinit_stokes); // Using StokesSolver
    std::vector<Real> vel1 = system.compute_point_velocity("disturbed");
    for(std::size_t j=0; j<vel1.size();++j) vel1[j] += vel0[j];
    brownian_sys.vector_transform(vel1, &U0, "forward");
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     * ---> test: output the particle velocity
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
//    const Real v0_min = v0_ptr->min();
//    const Real v0_max = v0_ptr->max();
//    const Real v0_sum = v0_ptr->sum();
//    if(comm_in.rank()==0){
//      for(unsigned int j=0; j<NP;++j)
//      {
//        std::vector<Real> vtest0(dim), vtest1(dim);
//        for(std::size_t k=0; k<dim;++k){
//          vtest0[k] = vel0[j*dim+k];
//          vtest1[k] = vel1[j*dim+k];
//        }
//        printf("--->test in test_move_particles(): velocity on the %u-th point:\n",j);
//        printf("            U0 = (%f,%f,%f)\n",   vtest0[0],vtest0[1],vtest0[2]);
//        printf("       U0 + U1 = (%f,%f,%f)\n\n", vtest1[0],vtest1[1],vtest1[2]);
//      }
//      printf("            v0_min = %f, v0_max = %f, v0_sum = %f)\n",v0_min,v0_max,v0_sum);
//    }
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     write out the equation systems
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    oss << "Write out particle-mesh equation system at step " << i+1 << ", time = " <<real_time;
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    if(i%write_interval==0) o_step++;
    if( write_es && (i%write_interval==0) )
    {
      system.add_local_solution();  // add local solution for the disturbed system
      system.solution->add(*v0_ptr);// add the undisturbed solution
#ifdef LIBMESH_HAVE_EXODUS_API
      exodus_ptr->append(true);
      exodus_ptr->write_timestep(out_filename+".e",system.get_equation_systems(),o_step,o_step);
      
      /*
       * This ExodusII_IO object is already associated with file: output_pm_system.e, 
       * cannot use it with requested file: output_pm_system.e-s.1
       * So we need reconstruct a new one.
       */
//      ExodusII_IO exodus(mesh);
//      oss << out_filename << "-s." << o_step << ".e" ;
//      exodus.write_equation_systems(oss.str(), equation_systems);
//      oss.str(""); oss.clear();
#endif
    }   // end if( write_es )
    
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     FIXME: Adaptive time step.
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    Real dt = dt0;    // inital time step
    Real vp_max = 0.0, vp_min = 0.0;
    for(unsigned int k=0; k<dim;++k) {
      vp_max += vel1[k]*vel1[k];
    }
    vp_min = vp_max;
    for(unsigned int j=1; j<NP;++j)
    {
      Real vp_norm = 0.0;
      for(std::size_t k=0; k<dim;++k) vp_norm += vel1[j*dim+k]*vel1[j*dim+k];
      vp_max = std::max(vp_max,vp_norm);
      vp_min = std::min(vp_min,vp_norm);
//      oss << "velocity magnitude of the bead "<<j<<" is "<<std::sqrt(vp_norm);
//      PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    }
    vp_max = std::sqrt(vp_max);     // maximum magnitude of particle velocity
    vp_min = std::sqrt(vp_min);
    if(with_brownian) {
      if(vp_max>1.0) dt /= vp_max;  // modify time step(non-dimensional bead radius = 1)
    }
    else {
      //const Real aa = 0.05;
      //if(vp_max*dt>aa*hmin) dt = aa*hmin/vp_max;
      if(vp_max>hmin) dt *= hmin/vp_max;
    }
    oss << "Max velocity magnitude is " << vp_max << ", hmin = " << hmin << "\n"
        << "Min velocity magnitude is " << vp_min << ". "
        << "The time increment at step "<< i+1 << " is dt = " << dt;
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Brownian displacement
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    if (with_brownian)
    {
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       Generate random vector dw whose mean = 0, variance = sqrt(2*dt)
       petsc_random_vector generates a uniform distribution [0 1] whose
       mean = 0.5 and variance = 1/12, so we need a shift and scale operation.
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      oss << "Generate random vector dw at step " << i+1;
      PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
      Real mean_dw = 0.0, variance_dw = 0.0;
      
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//      // Generate a uniform random vector ranging from 0 to 1 whose mean is close to 0.5.
//      //brownian_sys.petsc_random_vector(n_vec,&rand_ctx,&dw);
//      brownian_sys.std_random_vector(0.0,1.0,"uniform",&dw); // [0 1]
//      brownian_sys._vector_mean_variance(dw, mean_dw, variance_dw);
//      while ( std::abs(mean_dw-0.5)>=0.1 ) {
//        //brownian_sys.petsc_random_vector(n_vec,&rand_ctx,&dw);
//        brownian_sys.std_random_vector(0.0,1.0,"uniform",&dw);
//        brownian_sys._vector_mean_variance(dw, mean_dw, variance_dw);
//      }
//
//      // the mean is re-adjusted to 0 by substracting 0.5, and the variance by 12
//      VecShift(dw, -0.5);
//      const PetscScalar variance  = std::sqrt(2.0*dt*12.0);
//      VecScale(dw,variance);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      
      
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       A more precise way is to construct a random vector with gaussian distribution
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      const Real std_dev  = std::sqrt(dt);
      brownian_sys.std_random_vector(0.0,std_dev,"gaussian",&dw);
      brownian_sys._vector_mean_variance(dw, mean_dw, variance_dw);
      VecScale(dw,std::sqrt(2.0));
      
      
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       TEST: print out the mean and variance or view the generated vector.
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      PetscPrintf(PETSC_COMM_WORLD,
                  "--->test for     random_vector:        mean = %f, variance = %f\n",
                  mean_dw, variance_dw);
      PetscPrintf(PETSC_COMM_WORLD,
                  "Exact values for uniform distribution: mean = %f, variance = %f\n",
                  0.5, 1./12.);
      //PetscPrintf(PETSC_COMM_WORLD,"--->test random vector variance = %f, dw = \n",variance);
      //VecView(dw,PETSC_VIEWER_STDOUT_WORLD);  // View the random vector
      
      
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       Compute dw = B^-1 * dw using Chebyshev polynomial, dw will be changed!
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      oss << "Compute the mid-point Brownian displacement B^-1*dw at step " << i+1;
      PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
      VecCopy (dw,dw_mid);  // save dw to dw_mid, which will be used for Chebyshev
      for(std::size_t j=0; j<2; j++)
      {
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
         Compute the max/min eigenvalues if needed. Otherwise, magnify the interval.
         - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        if(compute_eigen){
          oss << "Compute the max & min eigenvalues for Chebyshev polynomial at step "<<i+1;
          PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
          brownian_sys.compute_eigenvalues(eig_min,eig_max,tol_eigen);
        }
        
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
         Compute the Brownian displacement B^-1 * dw using Chebyshev approximation.
         Here dw is both input and output variables, so it will be changed.
         - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        PetscPrintf(PETSC_COMM_WORLD,
                    "--->eig_min = %f, eig_max = %f, tol_cheb = %f, n_cheb = %d\n",
                    eig_min,eig_max,tol_cheb,n_cheb);
        cheb_converge = brownian_sys.chebyshev_polynomial_approximation(n_cheb,
                                                                        eig_min,eig_max,tol_cheb,&dw);
        
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
         If converged, dw returns the Brownian displacement, then break the j-loop;
         Otherwise, recompute eigenvalues
         - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        if(cheb_converge){
          compute_eigen = false; break;
        }
        else{
          compute_eigen = true;
          VecCopy(dw_mid,dw); /*copy back, recompute eigenvalues*/
          oss << "It is necessry to re-compute the eigenvalues at step " <<i+1;
          PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
        }
      } // end for j-loop
      
      
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       Double-check the convergence of Chebyshev polynomial approximation
       *** If cheb does NOT converge, consider re-generating a rand vector!
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      if(!cheb_converge)
      {
        oss << "****** Warning: Chebysheve failed to converge at step " <<i+1;
        PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
      }
      
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       magnify the spectral range by a factor (1.05 by default).
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      eig_max *= eig_factor; eig_min /= eig_factor;
      
      
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       Compute dw_mid = D*B^-1*dw, which can be obtained by solving the Stokes
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      oss << "Compute the mid-point Brownian displacement D*B^-1*dw at step " <<i+1;
      PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
      brownian_sys.hi_ewald(M,dw,dw_mid);  // dw_mid = D * dw
    } // end if (with_brownian)
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Particle coordinate vector R0.
     Move the particle R_mid = R0 + 0.5*(U0+U1)*dt (deterministic)
     and R_mid = R_mid + 0.5*sqrt(2)*D*B^-1*dw     (stochastic)
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    oss << "Update the mid-point coordinates at step " <<i+1;
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    brownian_sys.extract_particle_vector(&R0,"coordinate","extract");
    VecWAXPY(R_mid,0.5*dt,U0,R0);  // R_mid = R0 + 0.5*dt*(U0+U1)  (R0 and U0 do NOT change)
    if(with_brownian)
    {
      coef = 0.5;                    // coefficient. sqrt(2) is introduced when generating dw
      VecAXPY(R_mid,coef,dw_mid);    // R_mid = R_mid + 0.5*sqrt(2)*D*B^-1*dw
    }
    brownian_sys.extract_particle_vector(&R_mid,"coordinate","assign"); // Update mid-point coords
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Check and correct the beads' position at the midpoint
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    force_field.check_walls();
    bool chain_broken = polymer_chain.check_chain(max_spring_len);
    if(chain_broken) {
      oss << "********** warning: Polymer chain is broken at the step " <<i+1;
      PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    }
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Update the particle mesh for the mid-point step,
     and recompute U0 + U1_mid, D_mid*(B^-1*dw)
     NOTE: the FEM solution of undisturbed field doesn't change, but particles
     move, so U0 needs to be re-evaluated at the new position.
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    oss << "Compute the mid-point particle velocity at step " <<i+1;
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    *(system.solution) = *v0_ptr;       // re-assign the undisturbed solution
    system.update();
    system.reinit_system();
    vel0 = system.compute_point_velocity("undisturbed");
    reinit_stokes = false;
    system.solve_stokes("disturbed",reinit_stokes);   // solve the disturbed solution
    vel1 = system.compute_point_velocity("disturbed");
    for(std::size_t j=0; j<vel1.size();++j) vel1[j] += vel0[j];
    brownian_sys.vector_transform(vel1, &U0, "forward"); // (U0+U1)_mid
    if(with_brownian){
      brownian_sys.hi_ewald(M,dw,dw_mid);  // dw_mid = D_mid*dw, where dw=B^-1*dw computed above
    }
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     the mid-point to the NEW point, and update the particle coordinates
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    oss << "Update from mid-point to the NEW particle coordinates at step " <<i+1;
    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    VecWAXPY(R_mid,dt,U0,R0);         // R_mid = R0 + dt*U0_mid
    if(with_brownian){
      VecAXPY(R_mid,2.0*coef,dw_mid); // R_mid = R_mid + sqrt(2)*D_mid*B^-1*dw
    }
    brownian_sys.extract_particle_vector(&R_mid,"coordinate","assign");
    
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     (1) correct the particle position in the periodic direction and
     (2) prevent the particle moving out of the inpenetrable walls.
     (3) Check the polymer chain to make sure no chain is broken!
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
//    oss << "Correct particle positions and check the polymer chain at step " <<i+1;
//    PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
//    for (std::size_t j=0; j<NP; ++j){
//      point_mesh.pm_periodic_boundary()->
//      correct_position(point_mesh.particles()[j]->point());
//    }
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Check and correct the beads' position again after the midpoint update
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    force_field.check_walls();
    chain_broken = polymer_chain.check_chain(max_spring_len);
    if(chain_broken) {
      oss << "********** warning: Polymer chain is broken at the step " <<i+1;
      PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
    }
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     update the time
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    real_time += dt;
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     print out particle info if needed. If number of particles is large, we don't print.
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    if (NP<10)
    {
      oss << "Print the particle information at step " <<i+1;
      PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
      if (comm_in.rank() == 0)
      {
        point_mesh.print_point_info();
        //point_mesh.print_elem_neighbor_list();
      }
    }
    comm_in.barrier();
    
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Update ROUT (position vector excluding pbc) at the i-th step
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    VecAXPY(ROUT,dt,U0);              // ROUT = ROUT + dt*U0_mid
    if(with_brownian){
      VecAXPY(ROUT,2.0*coef,dw_mid);  // ROUT = ROUT + sqrt(2)*D_mid*B^-1*dw
    }
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     write out the polymer chain at the i-th step. Also output ROUT vector
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    if(i%write_interval==0)
    {
      /* output polymer chain data in the VTK format */
      oss << "output_polymer_" << o_step << ".vtk";
      if(comm_in.rank()==0) polymer_chain.write_polymer_chain( oss.str() );
      oss.str(""); oss.clear();
      
      // Write out ROUT for restart mode!
      PetscViewerBinaryOpen(PETSC_COMM_WORLD,"vector_ROUT.dat",FILE_MODE_WRITE,&viewer);
      VecView(ROUT,viewer);
    }
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Output mean square displacement(msd) and Radius of Gyration(Rg) at step i
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    const Point center1 = brownian_sys.center_of_mass(ROUT,dim);
    const Point msd     = brownian_sys.mean_square_displacement(center0,center1,dim);
    Rg                  = brownian_sys.radius_of_gyration(ROUT,center1,dim);
    chain_S             = brownian_sys.chain_stretch(ROUT,dim); //(stretch)
    if(comm_in.rank()==0)
    {
      out_msd.open("msd.txt",std::ios_base::app);
      out_msd.setf(std::ios::right);    out_msd.setf(std::ios::fixed);
      out_msd.precision(o_precision);   out_msd.width(o_width);
      out_msd << i+1 << " " << real_time << " ";                  // step | time |
      out_msd << msd(0) << " " << msd(1) << " " << msd(2) << " " << Rg << " ";// | msd_x | msd_y | msd_z | Rg
      out_msd << chain_S(0) << " " << chain_S(1) << " " << chain_S(2) << "\n";// Sx | Sy | Sz
      out_msd.close();
    }
    
  } // end for i-loop
  
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Destroy and return
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  oss << "The simulation is finished and Destroy all the PETSc objects.";
  PMToolBox::output_message(oss.str(), comm_in); oss.str(""); oss.clear();
  MatDestroy(&M);
  VecDestroy(&U0);
  VecDestroy(&R0);
  VecDestroy(&R_mid);
  VecDestroy(&dw_mid);
  PetscRandomDestroy(&rand_ctx);
  if(with_brownian){
    VecDestroy(&dw);
  }
  if(exodus_ptr) {
    delete exodus_ptr;
  }
  PetscViewerDestroy(&viewer);
  
  
  // return
  return 0;
}

#endif /* test_polymer_h */
