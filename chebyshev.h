//
//  chebyshev.h
//  
//
//  Created by Xujun Zhao on 8/11/15.
//
//

#ifndef ____chebyshev__
#define ____chebyshev__

#include <stdio.h>
#include <cmath>
#include <vector>


#include "libmesh/libmesh.h"
#include "libmesh/dense_matrix.h"
#include "libmesh/dense_vector.h"


namespace libMesh
{
  
  
  /*
   * The Chebyshev is designed for implementing
   * Chebyshev polynomial series
   */
 
//using libMesh::Real;
  
  
class Chebyshev
{
public:
  // Constructor
  Chebyshev();
  
  
  // Destructor
  ~Chebyshev();

  
  /*
   * The function of the Chebyshev expansion
   */
  Real chebyshev_function(const Real x,
                          const Real da_cheb,
                          const Real db_cheb) const;
  
  
  /*
   * Chebyshev expansion coefficients
   */
  DenseVector<Number> chebyshev_coefficients(const std::size_t N,
                                             const Real da_cheb,
                                             const Real db_cheb,
                                             const std::string method) const;
  
  
  /*
   * The transforming matrix from physical space to Chebyshev tranform space
   * using Gauss-Lotatto method
   */
  DenseMatrix<Number> transform_matrix(const std::size_t N) const;
  
  

  /* 
   * Quadrature points and weights to evaluate the Chebyshev coeffecients
   * (1) Chebyshev-Gauss: n = 0, 1, ... , N
   */
  void chebyshev_gauss(const std::size_t N,         // # of expansion terms
                       std::vector<Real>& x,        // quadrature points
                       std::vector<Real>& w) const; // weights


  /*
   * Quadrature points and weights to evaluate the Chebyshev coeffecients
   * (2) Chebyshev-Gauss-Radau: n = 0, 1, ... , N
   */
  void chebyshev_gauss_radau(const std::size_t N,         // # of expansion terms
                             std::vector<Real>& x,        // quadrature points
                             std::vector<Real>& w) const; // weights
  
  
  /*
   * Quadrature points and weights to evaluate the Chebyshev coeffecients
   * (3) Chebyshev-Gauss-Lobatto: n = 0, 1, ... , N
   */
  void chebyshev_gauss_lobatto(const std::size_t N,         // # of expansion terms
                               std::vector<Real>& x,        // quadrature points
                               std::vector<Real>& w) const; // weights
  
  
  

}; // end class Chebyshev



} // end namespace

#endif /* defined(____chebyshev__) */
