//
//  analytical_solution.h
//  
//
//  Created by Xujun Zhao on 5/2/16.
//
//

#ifndef analytical_solution_h
#define analytical_solution_h


#include <stdio.h>

#include "libmesh/reference_counted_object.h"

#include "pm_linear_implicit_system.h"

//namespace libMesh
//{

/*
 * This class defines some analytical solutions that are 
 * available for some special cases.
 *
 * This is only used for validation and test purpose.
 */




class AnalyticalSolution : public ReferenceCountedObject<AnalyticalSolution>
//public ParallelObject
{
public:

  // Constructor
  AnalyticalSolution(PMLinearImplicitSystem& pm_system);


  // Destructor
  ~AnalyticalSolution();
  
  
  // Exact solution for point forces in an unbounded domain
  std::vector<Real> exact_solution_infinite_domain(const Point& pt0) const;
  
  
  // correction factor for a particle in a cylinder: Bohlin approximation
  Real correction_factor_bohlin(const Real r_ratio) const;
  
  
  // correction factor for a particle in a cylinder: Haberman approximation
  Real correction_factor_haberman(const Real r_ratio) const;
  
  
  
private:
  
  PMLinearImplicitSystem& _pm_system;

}; // end of class defination



//}  // end of namespace

#endif /* analytical_solution_h */
